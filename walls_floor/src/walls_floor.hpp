#include <fea/structure.hpp>
#include <fea/render2d.hpp>
#include <fea/userinterface.hpp>
#include <drawables/walltilemap.hpp>
#include <drawables/floortilemap.hpp>

class Walls_floor : public fea::Application
{
    public:
        Walls_floor();
    protected:
        virtual void loop() override;
    private:
        void handleInput();
        fea::Window mWindow;
        fea::InputHandler mInputHandler;
        fea::Renderer2D mRenderer;

        fea::Texture mWallsTexture;
        WallTileMap mWallTileMap;
        fea::Texture mFloorsTexture;
        FloorTileMap mFloorTileMap;
        bool mHolding;
        bool mWalls;
        fea::Mouse::Button mButton;
};
