#include "topdown_renderer.hpp"
#include <fea/ui/sdl2windowbackend.hpp>
#include <fea/ui/sdl2inputbackend.hpp>
#include "resources/textures.hpp"
#include "resources/entitygfxes.hpp"
#include "texturemaker.hpp"
#include <iostream>
#include <functional>

Topdown_renderer::Topdown_renderer() :
    mWindow(new fea::SDL2WindowBackend(), fea::VideoMode(1024, 768), "Topdown_renderer"),
    mInputHandler(new fea::SDL2InputBackend()),
    mRenderer(fea::Viewport({1024, 768}, {0, 0}, fea::Camera({1024 / 2.0f, 768 / 2.0f}))),
    mAnimatedObjectProvider(mTextures),
    mBlobPosition(500.0f, 400.0f),
    mClicked(false)
{
    mWindow.setVSyncEnabled(true);
    mWindow.setFramerateLimit(60);

    mRenderer.getViewport().getCamera().zoom({2.0f, 2.0f});

    std::hash<std::string> stringHasher;

    for(const auto& texture : rTextures)
    {
        std::cout << "loading texture '" << texture.first << "' from '" << texture.second.texturePath << "'\n";
        int32_t id = static_cast<int32_t>(stringHasher(texture.first));
        mTextures.emplace(id, makeTexture(texture.second.texturePath));
    }

    for(const auto& entityGfx : rEntityGfxes)
    {
        std::cout << "loading entity graphics: " << entityGfx.first << "\n";

        const glm::ivec2& size = entityGfx.second.size;
        int32_t textureId = static_cast<int32_t>(stringHasher(entityGfx.second.texture));
        int32_t defaultAnimation = static_cast<int32_t>(stringHasher(entityGfx.second.defaultAnimation));

        std::unordered_map<int32_t, fea::Animation> animations;

        for(const auto& animation : entityGfx.second.animations)
        {
            int32_t animationId = static_cast<int32_t>(stringHasher(animation.first));
            fea::Animation newAnim(animation.second.start, animation.second.size, static_cast<uint32_t>(animation.second.frameCount), static_cast<uint32_t>(animation.second.delay), animation.second.loop);

            animations.emplace(animationId, std::move(newAnim));
        }

        mAnimatedObjectProvider.registerAnimatedObjectType(static_cast<int32_t>(stringHasher(entityGfx.first)), {textureId, defaultAnimation, (glm::vec2)size, {0, 0}, std::move(animations)});
    }

    mMouseObject = mAnimatedObjectProvider.createAnimatedObject(static_cast<int32_t>(stringHasher("star")), {0.0f, 0.0f});
    mBlobObject = mAnimatedObjectProvider.createAnimatedObject(static_cast<int32_t>(stringHasher("blob")), mBlobPosition);
}

void Topdown_renderer::loop()
{
    handleInput();

    glm::vec2 blobDirection = mBlobTarget - mBlobPosition;
    if(glm::distance(mBlobTarget, mBlobPosition) > 10.0f)
    {
        mBlobPosition += glm::normalize(blobDirection) * 2.0f * (mClicked ? -1.0f : 1.0f);
        mAnimatedObjectProvider.setAnimatedObjectPosition(mBlobObject, mBlobPosition);
    }

    float xDiff = std::fabs(mBlobPosition.x - mBlobTarget.x);
    float yDiff = std::fabs(mBlobPosition.y - mBlobTarget.y);

    std::hash<std::string> stringHasher;

    if(xDiff < yDiff)
    {
        if(blobDirection.y > 0.0f)
            mAnimatedObjectProvider.setAnimatedObjectAnimation(mBlobObject, static_cast<int32_t>(stringHasher("walk_down")));
        else
            mAnimatedObjectProvider.setAnimatedObjectAnimation(mBlobObject, static_cast<int32_t>(stringHasher("walk_up")));
    }
    else
    {
        if(blobDirection.x > 0.0f)
            mAnimatedObjectProvider.setAnimatedObjectAnimation(mBlobObject, static_cast<int32_t>(stringHasher("walk_right")));
        else
            mAnimatedObjectProvider.setAnimatedObjectAnimation(mBlobObject, static_cast<int32_t>(stringHasher("walk_left")));
    }


//    mRenderer.clear();
//    mRenderer.queue(mAnimatedObjectProvider);
   mAnimatedObjectProvider.render(mRenderer);
//    mRenderer.render();
    mAnimatedObjectProvider.tick(1);

    mWindow.swapBuffers();
}

void Topdown_renderer::handleInput()
{
    std::hash<std::string> stringHasher;

    fea::Event event;
    while(mInputHandler.pollEvent(event))
    {
        if(event.type == fea::Event::KEYPRESSED)
        {
            if(event.key.code == fea::Keyboard::ESCAPE)
                quit();
        }
        else if(event.type == fea::Event::CLOSED)
        {
            quit();
        }
        else if(event.type == fea::Event::RESIZED)
        {
            //float w = (float)event.size.width / 2.0f;
            //float h = (float)event.size.height / 2.0f;
            //mRenderingSystem.setViewport(fea::Viewport({event.size.width, event.size.height}, {0, 0}, fea::Camera({w, h}))); //message
        }
        else if(event.type == fea::Event::MOUSEBUTTONPRESSED)
        {
            mAnimatedObjectProvider.setAnimatedObjectAnimation(mMouseObject, static_cast<int32_t>(stringHasher("break")));
            mClicked = true;
        }
        else if(event.type == fea::Event::MOUSEBUTTONRELEASED)
        {
            //
        }
        else if(event.type == fea::Event::MOUSEMOVED)
        {
            mAnimatedObjectProvider.setAnimatedObjectPosition(mMouseObject, glm::vec2(event.mouseMove.x, event.mouseMove.y));
            mBlobTarget = {event.mouseMove.x, event.mouseMove.y};
        }
    }
}
