#include <fea/structure.hpp>
#include <fea/render2d.hpp>
#include <fea/userinterface.hpp>
#include "renderingsystem/animatedobjectprovider.hpp"

class Topdown_renderer : public fea::Application
{
    public:
        Topdown_renderer();
    protected:
        virtual void loop() override;
    private:
        void handleInput();
        fea::Window mWindow;
        fea::InputHandler mInputHandler;
        fea::Renderer2D mRenderer;

        TextureMap mTextures;
        AnimatedObjectProvider mAnimatedObjectProvider;
        int32_t mMouseObject;
        int32_t mBlobObject;
        glm::vec2 mBlobPosition;
        glm::vec2 mBlobTarget;
        bool mClicked;
};
