#include "guielement.hpp"

GuiElement::GuiElement():
    mElement(nullptr)
{
}

GuiElement::GuiElement(const gim::Element& element):
    mElement(&element)
{
}

void GuiElement::addFont(gim::Font& font)
{
    mFonts.emplace_back(font);

    std::unique_ptr<fea::Texture> newTexture = std::make_unique<fea::Texture>();
    auto& created = mFontTextures.emplace(newTexture->getId(), std::move(newTexture)).first->second;
    mRenderDataGenerator.registerFontStorage({font}, FeaTextureAdaptor(*created));
}

std::vector<fea::RenderEntity> GuiElement::getRenderInfo() const
{
    std::vector<fea::RenderEntity> result;

    auto renderData = mRenderDataGenerator.generate(*mElement);

    for(const auto& elementData : renderData)
    {
        if(!elementData.positions.empty())
        {
            fea::RenderEntity panelEntity;

            panelEntity.mDrawMode = GL_TRIANGLES;

            std::vector<float> positions = discardZ(elementData.positions);

            panelEntity.mVertexAttributes.emplace("vertex", fea::VertexAttribute(2, positions));
            panelEntity.mVertexAttributes.emplace("colors", fea::VertexAttribute(4, elementData.colors));
            panelEntity.mVertexAttributes.emplace("texCoords", fea::VertexAttribute(2, elementData.texCoords));

            panelEntity.mUniforms.emplace("position", fea::Uniform(fea::VEC2, glm::vec2()));
            panelEntity.mUniforms.emplace("texture", fea::Uniform(fea::TEXTURE, (int)elementData.texture->handle()));
            panelEntity.mUniforms.emplace("origin", fea::Uniform(fea::VEC2, glm::vec2()));
            panelEntity.mUniforms.emplace("rotation", fea::Uniform(fea::FLOAT, 0.0f));
            panelEntity.mUniforms.emplace("scaling", fea::Uniform(fea::VEC2, glm::vec2(1.0f, 1.0f)));
            panelEntity.mUniforms.emplace("parallax", fea::Uniform(fea::VEC2, glm::vec2(1.0f, 1.0f)));
            panelEntity.mUniforms.emplace("constraints", fea::Uniform(fea::VEC4, glm::vec4(0.0f, 1.0f, 0.0f, 1.0f)));
            panelEntity.mUniforms.emplace("color", fea::Uniform(fea::VEC3, glm::vec3(1.0f, 1.0f, 1.0f)));
            panelEntity.mUniforms.emplace("opacity", fea::Uniform(fea::FLOAT, 1.0f));
            panelEntity.mElementAmount = static_cast<uint32_t>(positions.size() / 2);

            result.emplace_back(std::move(panelEntity));
        }

        if(!elementData.textPositions.empty())
        {
            fea::RenderEntity textEntity;

            std::vector<float> positions = discardZ(elementData.textPositions);

            textEntity.mDrawMode = GL_TRIANGLES;
            textEntity.mVertexAttributes.emplace("vertex", fea::VertexAttribute(2, positions));
            textEntity.mVertexAttributes.emplace("colors", fea::VertexAttribute(4, elementData.textColors));
            textEntity.mVertexAttributes.emplace("texCoords", fea::VertexAttribute(2, elementData.textTexCoords));

            textEntity.mUniforms.emplace("position", fea::Uniform(fea::VEC2, glm::vec2()));
            textEntity.mUniforms.emplace("texture", fea::Uniform(fea::TEXTURE, (int)elementData.textTexture->handle()));
            textEntity.mUniforms.emplace("origin", fea::Uniform(fea::VEC2, glm::vec2()));
            textEntity.mUniforms.emplace("rotation", fea::Uniform(fea::FLOAT, 0.0f));
            textEntity.mUniforms.emplace("scaling", fea::Uniform(fea::VEC2, glm::vec2(1.0f, 1.0f)));
            textEntity.mUniforms.emplace("parallax", fea::Uniform(fea::VEC2, glm::vec2(1.0f, 1.0f)));
            textEntity.mUniforms.emplace("constraints", fea::Uniform(fea::VEC4, glm::vec4(0.0f, 1.0f, 0.0f, 1.0f)));
            textEntity.mUniforms.emplace("color", fea::Uniform(fea::VEC3, glm::vec3(1.0f, 1.0f, 1.0f)));
            textEntity.mUniforms.emplace("opacity", fea::Uniform(fea::FLOAT, 1.0f));
            textEntity.mElementAmount = static_cast<uint32_t>(positions.size() / 2);

            result.emplace_back(std::move(textEntity));
        }
    }

    return result;
}

void GuiElement::registerTexture(const std::string name, fea::Texture& texture)
{
    mRenderDataGenerator.registerTexture(name, FeaTextureAdaptor(texture));
}

void GuiElement::setElement(const gim::Element& element)
{
    mElement = &element;   
}

std::vector<float> GuiElement::discardZ(const std::vector<float>& positions) const
{
    std::vector<float> result;
    result.reserve(positions.size());

    int32_t counter = 0;

    for(float coord : positions)
    {
        if(counter != 2)
        {
            result.emplace_back(coord);
            counter++;
        }
        else
            counter = 0;
    }

    return result;
}
