#pragma once
#include "../util/grid.hpp"

enum FloorVariation {
    VARIATION1,
    VARIATION2,
    VARIATION3,
    VARIATION4,
};

using FloorType = int32_t;
using TypeVariationHash = int32_t;

class FloorTileMap
{
    public:
        FloorTileMap(const glm::ivec2& tileSize, const glm::ivec2& textureUnitSize, int32_t chunkSize = 32);
        FloorTileMap(const glm::ivec2& mapSize, const glm::ivec2& tileSize, const glm::ivec2& textureUnitSize, int32_t chunkSize = 32);
        void reset(const Grid<FloorType>& floors);
        void setTexture(const fea::Texture& texture);
        void defineType(FloorType type, const glm::ivec2& textureCoordinates);
        void set(const glm::ivec2& coordinate, FloorType type);
        void unset(const glm::ivec2& coordinate);
        const std::vector<fea::RenderEntity>& getRenderInfo() const;
        void setCullRegion(const glm::vec2& start, const glm::vec2& end);
        void setCullEnabled(bool enabled);
    private:
        TypeVariationHash typeVariationHasher(FloorType type, FloorVariation floorVariation) const;
        uint64_t coordinateHasher(const glm::ivec2& coordinate) const;
        void addTileDefinition(FloorType type, FloorVariation variation, const glm::ivec2& coordinate);
        FloorType typeAt(const glm::ivec2& coordinate) const;
        FloorType typeAtGfxTile(const glm::ivec2& coordinate) const;
        void updateTiles(const glm::ivec2& coordinate);
        bool isWithin(const glm::ivec2& coordinate) const;
        fea::TileMap mTiles;
        Grid<FloorType> mFloorTypes;
        std::unordered_set<FloorType> mAddedTypes;
        std::unordered_map<TypeVariationHash, glm::ivec2> mTypeDefinitions;
};
