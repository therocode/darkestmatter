#pragma once
#include "../util/grid.hpp"

enum WallDirection {
    ES,
    ESW1,
    ESW2,
    ESW3,
    SW,
    NES1,
    NESW1,
    ESC,
    SWC,
    NSW1,
    NES2,
    NESW2,
    NEC,
    NWC,
    NSW2,
    NE,
    NEW1,
    NEW2,
    NEW3,
    NW,
};

using WallType = int32_t;
using TypeDirectionHash = int32_t;

class WallTileMap
{
    public:
        WallTileMap(const glm::ivec2& tileSize, const glm::ivec2& textureUnitSize, int32_t chunkSize = 32);
        WallTileMap(const glm::ivec2& mapSize, const glm::ivec2& tileSize, const glm::ivec2& textureUnitSize, int32_t chunkSize = 32);
        void reset(const Grid<WallType>& walls);
        void setTexture(const fea::Texture& texture);
        void defineType(WallType type, const glm::ivec2& textureCoordinates);
        void set(const glm::ivec2& coordinate, WallType type);
        void unset(const glm::ivec2& coordinate);
        const std::vector<fea::RenderEntity>& getRenderInfo() const;
        void setCullRegion(const glm::vec2& start, const glm::vec2& end);
        void setCullEnabled(bool enabled);
    private:
        TypeDirectionHash typeDirectionHasher(WallType type, WallDirection wallDirection) const;
        uint64_t coordinateHasher(const glm::ivec2& coordinate) const;
        void addTileDefinition(WallType type, WallDirection direction, const glm::ivec2& coordinate);
        WallType typeAt(const glm::ivec2& coordinate) const;
        WallType typeAtGfxTile(const glm::ivec2& coordinate) const;
        void updateDirection(const glm::ivec2& coordinate);
        bool isWithin(const glm::ivec2& coordinate) const;
        fea::TileMap mTiles;
        Grid<WallType> mWallTypes;
        std::unordered_set<WallType> mAddedTypes;
        std::unordered_map<TypeDirectionHash, glm::ivec2> mTypeDefinitions;
};
