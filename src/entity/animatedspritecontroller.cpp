#include "animatedspritecontroller.hpp"
#include "world/worldmessages.hpp"

AnimatedSpriteController::AnimatedSpriteController(fea::MessageBus& bus):
    mBus(bus)
{
    subscribe(mBus, *this);
}

bool AnimatedSpriteController::keepEntity(fea::EntityPtr entity) const
{
    bool keep = entity->hasAttribute("position") &&
                entity->hasAttribute("sprite_id");

    return keep;
}

void AnimatedSpriteController::entityKept(fea::EntityPtr entity)
{
    auto sprite = entity->getAttribute<int32_t>("sprite_id");   
    auto position = entity->getAttribute<glm::vec2>("position");

    mBus.send(AnimatedSpriteCreatedMessage{entity->getId(), position, sprite});
}

void AnimatedSpriteController::entityDestroyed(fea::EntityPtr entity)
{
    mBus.send(AnimatedSpriteDestroyedMessage{entity->getId()});
}

void AnimatedSpriteController::handleMessage(const EntityMovedMessage& message)
{
    mBus.send(AnimatedSpriteMovedMessage{message.entityId, message.position});
}
