#pragma once
#include <thero/smartenum.hpp>

smart_enum_class(JobPriority, NORMAL, NORMAL_INTERRUPT, IMPORTANT, IMPORTANT_INTERRUPT, EMERGENCY, EMERGENCY_INTERRUPT)
