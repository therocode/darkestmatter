#pragma once
#include <fea/entitysystem.hpp>
#include <fea/util.hpp>
#include <fea/rendering/glmhash.hpp>
#include "entity/jobid.hpp"
#include "jobai/job.hpp"
#include "world/worldmessages.hpp"

class GameWorld;
class TaskAIController;

class EvadeTileController: public fea::EntityController, public fea::MessageReceiver<
    JobFinishedMessage,
    JobFailedMessage
    >
{
    struct EvadeJobEntry
    {
        JobId id;
        Job job;
    };
    public:
        EvadeTileController(fea::MessageBus& bus);
        bool keepEntity(fea::EntityPtr entity) const override;
        void entityKept(fea::EntityPtr entity) override;
        void entityDestroyed(fea::EntityPtr entity) override;
        void update(float deltaTime) override;
        std::unordered_set<glm::ivec2>& evadeTiles();
        const std::unordered_set<glm::ivec2>& evadeTiles() const; //this will be a problem if several things set the same evade tile. A refcounted system is better...
        void setTaskAIController(TaskAIController& taskAIController);
        void handleMessage(const JobFinishedMessage& message) override;
        void handleMessage(const JobFailedMessage& message) override;
    private:
        void failEvasion(fea::EntityId entity);
        fea::MessageBus& mBus;
        TaskAIController* mTaskAIController;
        std::unordered_set<glm::ivec2> mEvadeTiles;
        std::unordered_map<fea::EntityId, EvadeJobEntry> mEvadeJobs;
        std::unordered_map<fea::EntityId, int32_t> mFailCooldown;
};
