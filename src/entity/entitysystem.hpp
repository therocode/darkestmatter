#pragma once
#include <fea/entitysystem.hpp>
#include <memory>

class EntitySystem 
{
    public:
        ~EntitySystem();
        fea::EntityController& addController(std::unique_ptr<fea::EntityController> controller);
        void entityCreated(const fea::EntityPtr entity);
        void entityRemoved(fea::EntityId id);
        void update(float deltaTime);
    private:
        std::vector<std::unique_ptr<fea::EntityController>> mControllers;
};
