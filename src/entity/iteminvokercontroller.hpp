#pragma once
#include <thero/optional.hpp>
#include <fea/entitysystem.hpp>
#include <fea/util.hpp>
#include "world/itemmanager.hpp"

class GameWorld;

class ItemInvokerController: public fea::EntityController
{
    public:
        ItemInvokerController(fea::MessageBus& bus, ItemManager& itemManager);
        virtual bool keepEntity(fea::EntityPtr entity) const override;
        virtual void entityKept(fea::EntityPtr entity) override;
        virtual void entityDestroyed(fea::EntityPtr entity) override;
        virtual void update(float deltaTime) override;
        th::Optional<AnyMap<std::string>> invokeItem(fea::EntityId entityId, Item& item, ItemAction action, AnyMap<std::string> parameters);
        th::Optional<AnyMap<std::string>> invokeItemStack(fea::EntityId entityId, int32_t stackId, int32_t stackIndex, ItemAction action, AnyMap<std::string> parameters);
    private:
        fea::MessageBus& mBus;
        ItemManager& mItemManager;
};
