#include "entitysystem.hpp"

EntitySystem::~EntitySystem()
{
    for(auto& controller : mControllers)
    {
        controller.reset();
    }
}

fea::EntityController& EntitySystem::addController(std::unique_ptr<fea::EntityController> controller)
{
    mControllers.push_back(std::move(controller));

    return *mControllers.back();
}

void EntitySystem::entityCreated(const fea::EntityPtr createdEntity)
{
    for(auto& controller : mControllers)
    {
        controller->entityCreated(createdEntity);
    }
}

void EntitySystem::entityRemoved(fea::EntityId id)
{
    for(auto& controller : mControllers)
    {
        controller->entityRemoved(id);
    }
}

void EntitySystem::update(float deltaTime)
{
    for(auto& controller : mControllers)
    {
        controller->update(deltaTime);
    }
}
