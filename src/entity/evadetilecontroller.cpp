#include "evadetilecontroller.hpp"
#include "world/tiles.hpp"
#include "world/gameworld.hpp"
#include "world/landscapesemantics.hpp"
#include "entity/entitymessages.hpp"
#include "entity/taskaicontroller.hpp"
#include "facility/jobs.hpp"

EvadeTileController::EvadeTileController(fea::MessageBus& bus):
    mBus(bus),
    mTaskAIController(nullptr)
{
    subscribe(mBus, *this);
}

bool EvadeTileController::keepEntity(fea::EntityPtr entity) const
{
    bool keep = entity->hasAttribute("work_status");

    return keep;
}

void EvadeTileController::entityKept(fea::EntityPtr entity)
{
}

void EvadeTileController::entityDestroyed(fea::EntityPtr entity)
{
    
}

void EvadeTileController::update(float deltaTime)
{
    for(auto iter = mFailCooldown.begin(); iter != mFailCooldown.end();)
    {//update cooldowns
        --iter->second;

        if(iter->second <= 0)
            iter = mFailCooldown.erase(iter);
        else
            ++iter;
    }

    if(!mEvadeTiles.empty())
    {//if there are evade tiles, we need to do things
        for(auto entityIter : mEntities)
        {
            auto entity = entityIter.second;
            auto jobIter = mEvadeJobs.find(entity->getId());

            if(jobIter == mEvadeJobs.end())
            {//if an entity does not have an existing entry...
                const auto& position = entity->getAttribute<glm::vec2>("position");

                if(mFailCooldown.count(entity->getId()) == 0)
                {///...and has not failed recently, we need to check if it overlaps an evade tile

                    //find out the corners of the entity
                    float halfWidth = entity->getAttribute<float>("width") / 2.0f;

                    glm::vec2 tl = position + glm::vec2(-halfWidth, -halfWidth);
                    glm::vec2 tr = position + glm::vec2( halfWidth, -halfWidth);
                    glm::vec2 bl = position + glm::vec2(-halfWidth,  halfWidth);
                    glm::vec2 br = position + glm::vec2( halfWidth,  halfWidth);

                    bool mustEvade = false;
                    glm::ivec2 evadeStart;

                    for(const auto& tile : mEvadeTiles)
                    {
                        if(Tiles::withinTile(tl, tile) ||
                                Tiles::withinTile(tr, tile) ||
                                Tiles::withinTile(bl, tile) ||
                                Tiles::withinTile(br, tile))
                        {//there is an overlap!
                            mustEvade = true;
                            break;
                        }

                    }

                    if(mustEvade)
                    {//okay, there is an overlap, so we need to create a task for it
                        FEA_ASSERT(mTaskAIController, "We don't have a taskAIController");

                        auto newJob = mEvadeJobs.emplace(entity->getId(), EvadeJobEntry{0, Job{Jobs::EVADE_TILES, {{"evade_tiles", mEvadeTiles}}}}).first; //0 because it is filled in later

                        auto newJobId = mTaskAIController->doJob(entity->getId(), newJob->second.job, JobPriority::IMPORTANT_INTERRUPT); //this is quite important!

                        if(newJobId)
                        {//the job is accepted and we must store its ID
                            newJob->second.id = *newJobId; //0 from before is filled in here
                        }
                        else
                        {//the brain refused to solve the task. It's a fail for now
                            mEvadeJobs.erase(entity->getId());
                            failEvasion(entity->getId());
                        }
                    }
                }
            }
            else
            {//this entity has an entry, which means it is trying to avoid it already
            }
        }
    }
    else
    {
    }
}

std::unordered_set<glm::ivec2>& EvadeTileController::evadeTiles()
{
    return mEvadeTiles;
}

const std::unordered_set<glm::ivec2>& EvadeTileController::evadeTiles() const
{
    return mEvadeTiles;
}

void EvadeTileController::setTaskAIController(TaskAIController& taskAIController)
{
    mTaskAIController = &taskAIController;
}

void EvadeTileController::handleMessage(const JobFinishedMessage& message)
{
    auto evadeJobsIter = mEvadeJobs.find(message.entity->getId());

    if(evadeJobsIter != mEvadeJobs.end())
    {
        if(evadeJobsIter->second.id == message.jobId)
        {
            mEvadeJobs.erase(evadeJobsIter);
            std::cout << "succeeded evade job from brain. Now evade jobs are: " << mEvadeJobs.size() << "\n";
        }
    }
}

void EvadeTileController::handleMessage(const JobFailedMessage& message)
{
    auto evadeJobsIter = mEvadeJobs.find(message.entity->getId());

    if(evadeJobsIter != mEvadeJobs.end())
    {
        if(evadeJobsIter->second.id == message.jobId)
        {
            mEvadeJobs.erase(evadeJobsIter);
            failEvasion(message.entity->getId());
            std::cout << "failed evade job from brain. Now evade jobs are: " << mEvadeJobs.size() << "\n";
        }
    }
}

void EvadeTileController::failEvasion(fea::EntityId entity)
{
    mFailCooldown[entity] = 50 + rand() % 10; //using rand() to just slightly space out retries. The drawbacks of rand() don't matter here
}
