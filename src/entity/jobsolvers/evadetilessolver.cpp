#include "evadetilessolver.hpp"
#include "world/gameworld.hpp"
#include "world/tiles.hpp"
#include "entity/entitymessages.hpp"
#include "world/landscapesemantics.hpp"
#include "facility/jobs.hpp"

EvadeTilesSolver::EvadeTilesSolver(const Job& job, const AnyMap<std::string>& external):
    JobSolver::JobSolver(job, external),
    mBus(external.at("message_bus").get<std::reference_wrapper<fea::MessageBus>>()),
    mWorld(external.at("world").get<std::reference_wrapper<GameWorld>>()),
    mCarryController(external.at("carry_controller").get<std::reference_wrapper<CarryController>>()),
    mEntity(external.at("entity").get<fea::EntityPtr>()),
    mEvadeTiles(job.jobData.at("evade_tiles").get<std::unordered_set<glm::ivec2>>()),
    mAtTarget(false),
    mNoPath(false)
{
}

JobSolver::UpdateResult EvadeTilesSolver::update()
{
    float acceptableDistance = glm::length((glm::vec2)Tiles::size())/8.f;
    if(mAtTarget)
        return succeed();
    if(mNoPath)
        return fail();

    //I must find a tile to evade to
    auto position = mEntity->getAttribute<glm::vec2>("position");
    glm::ivec2 tilePosition = Tiles::toTile(position);

    //algorithm to find a nearby non-evade tile
    std::queue<glm::ivec2> toInvestigate;
    toInvestigate.push(tilePosition);

    std::unordered_set<glm::ivec2> rejected;

    th::Optional<glm::ivec2> tileToGoTo;

    while(!toInvestigate.empty())
    {
        glm::ivec2 tileToCheck = toInvestigate.front();
        toInvestigate.pop();

        //see if this tile is a valid tile to evade to or not
        if(mEvadeTiles.count(tileToCheck) ||
                rejected.count(tileToCheck))

        {
            rejected.insert(tileToCheck);
        }
        else
        {
            tileToGoTo.reset(tileToCheck);
            break;
        }

        //see which neighbours to check
        std::array<glm::ivec2, 4> neighbours =
        {{
             tileToCheck + glm::ivec2(0, -1),
             tileToCheck + glm::ivec2(1, 0),
             tileToCheck + glm::ivec2(0, 1),
             tileToCheck + glm::ivec2(-1, 0) 
         }};

        for(const auto& neighbour : neighbours)
        {
            if(mWorld.landscape().inBounds(neighbour) && !LandscapeSemantics::isSolid(mWorld.landscape().walls().at(neighbour)))
            {
                toInvestigate.push(neighbour);
            }
        }
    }

    if(!tileToGoTo)
    {//there is no tile to escape to
        return fail();
    }
    else
    {//there is a tile, let's go there
        return require({Jobs::GOTO, {{"position", Tiles::tileCenter(*tileToGoTo)}, {"minimumDistance", acceptableDistance}}});
    }

    return working();
}

void EvadeTilesSolver::notifySuccess(AnyMap<std::string> completedData)
{
    //my goto has succeeded. I should be done now
    mAtTarget = true;
}

void EvadeTilesSolver::notifyFail(AnyMap<std::string> failedData)
{
    //oh no
    mNoPath = true;
}
