#include "entity/jobsolvers/fromstacktostacksolver.hpp"
#include "world/gameworld.hpp"
#include "facility/jobs.hpp"
#include "world/tiles.hpp"

FromStackToStackSolver::FromStackToStackSolver(const Job& job, const AnyMap<std::string>& external):
    JobSolver::JobSolver(job, external),
    mBus(external.at("message_bus").get<std::reference_wrapper<fea::MessageBus>>()),
    mWorld(external.at("world").get<std::reference_wrapper<GameWorld>>()),
    mEntity(external.at("entity").get<fea::EntityPtr>()),
    mItemTypeId(job.jobData.at("item_type_id").get<int32_t>()),
    mFromStackId(job.jobData.at("from_item_stack_id").get<int32_t>()),
    mToStackPosition(job.jobData.at("to_item_stack_position").get<glm::ivec2>()),
    mFetched(false),
    mPutted(false),
    mCannotFetch(false),
    mCannotPut(false)
{
}

JobSolver::UpdateResult FromStackToStackSolver::update()
{
    if(mCannotFetch || mCannotPut)
        return fail();

    if(mPutted)
        return succeed();

    if(mFetched)
    {
       return require({Jobs::PUT_ITEM_ON_STACK,
        {
            {"item_type_id", mItemTypeId},
            {"to_item_stack_position", mToStackPosition},
        }});
    }
    else
    {
        return require({Jobs::FETCH_FROM_STACK,
        {
            {"item_type_id", mItemTypeId},
            {"from_item_stack_id", mFromStackId},
        }});
    }
}

void FromStackToStackSolver::notifySuccess(AnyMap<std::string> completedData)
{
    if(mRequiredJob.type == Jobs::FETCH_FROM_STACK)
    {
        mFetched = true;
    }
    else if(mRequiredJob.type == Jobs::PUT_ITEM_ON_STACK)
    {
        mCannotPut = true;
    }
}

void FromStackToStackSolver::notifyFail(AnyMap<std::string> failedData)
{
    if(mRequiredJob.type == Jobs::FETCH_FROM_STACK)
    {
        mCannotFetch = true;
    }
    else if(mRequiredJob.type == Jobs::PUT_ITEM_ON_STACK)
    {
        mCannotPut = true;
    }
}
