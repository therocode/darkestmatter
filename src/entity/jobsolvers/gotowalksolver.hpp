#pragma once
#include <deque>
#include <fea/util.hpp>
#include <thero/optional.hpp>
#include "jobai/jobsolver.hpp"
#include "world/landscape.hpp"
#include "world/landscapepathadaptor.hpp"

class GameWorld;

class GotoWalkSolver : public JobSolver
{
    public:
        GotoWalkSolver(const Job& job, const AnyMap<std::string>& external);
        ~GotoWalkSolver();
        UpdateResult update() override;
        void notifySuccess(AnyMap<std::string> completedData) override;
        void notifyFail(AnyMap<std::string> failedData) override;
    private:
        fea::MessageBus& mBus;
        GameWorld& mWorld;
        fea::EntityPtr mEntity;
        glm::vec2 mTargetPosition;
        float     mMinimumDistance;

        fea::Pathfinder<LandscapePathAdaptor> mPathfinder;
        th::Optional<fea::Pathfinder<LandscapePathAdaptor>::Path> mPath;

        std::deque<float> mDistanceTravelledLately;
        glm::vec2 mLastPosition;
};
