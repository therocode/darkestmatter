#pragma once
#include "jobai/jobsolver.hpp"
#include "entity/carrycontroller.hpp"
#include "world/itemmanager.hpp"

class GameWorld;

class FromStackToStackSolver : public JobSolver
{
    public:
        FromStackToStackSolver(const Job& job, const AnyMap<std::string>& external);
        UpdateResult update() override;
        void notifySuccess(AnyMap<std::string> completedData) override;
        void notifyFail(AnyMap<std::string> failedData) override;
    private:
        fea::MessageBus& mBus;
        GameWorld& mWorld;
        fea::EntityPtr mEntity;

        int32_t mItemTypeId;
        int32_t mFromStackId;
        glm::ivec2 mToStackPosition;

        bool mFetched;
        bool mPutted;
        bool mCannotFetch;
        bool mCannotPut;
};
