#pragma once
#include "jobai/jobsolver.hpp"
#include "world/itemmanager.hpp"
#include "entity/carrycontroller.hpp"

class GameWorld;

class StorageKeepingSolver : public JobSolver
{
    public:
        StorageKeepingSolver(const Job& job, const AnyMap<std::string>& external);
        UpdateResult update() override;
        void notifySuccess(AnyMap<std::string> completedData) override;
        void notifyFail(AnyMap<std::string> failedData) override;
    private:
        fea::MessageBus& mBus;
        GameWorld& mWorld;
        CarryController& mCarryController;
        fea::EntityPtr mEntity;

        ItemManager::ItemStackList mStashableStacks;
        ItemManager::ItemStackList::iterator mCurrentStashable;
        ItemManager::ItemStackList mContainerStacks;
        ItemManager::ItemStackList::iterator mCurrentContainer;

        bool mPutSomethingAway;
        bool mFailed;
};
