#include "fromstacktocontainersolver.hpp"
#include "world/gameworld.hpp"
#include "facility/jobs.hpp"
#include "world/tiles.hpp"

FromStackToContainerSolver::FromStackToContainerSolver(const Job& job, const AnyMap<std::string>& external):
    JobSolver::JobSolver(job, external),
    mBus(external.at("message_bus").get<std::reference_wrapper<fea::MessageBus>>()),
    mWorld(external.at("world").get<std::reference_wrapper<GameWorld>>()),
    mEntity(external.at("entity").get<fea::EntityPtr>()),
    mItemTypeId(job.jobData.at("item_type_id").get<int32_t>()),
    mItemStackId(job.jobData.at("from_item_stack_id").get<int32_t>()),
    mContainerId(job.jobData.at("to_container_stack_id").get<int32_t>()),
    mFetchedItem(false),
    mCannotFetch(false),
    mPutItem(false)
{
}

JobSolver::UpdateResult FromStackToContainerSolver::update()
{
    if(mCannotFetch)
        return fail();

    if(mPutItem)
        return succeed();

    if(!mFetchedItem)
    {//I have not fetched the target item, I must do it
        if(!mWorld.items().stack(mItemStackId))
        {//item stack invalidated
            return fail();
        }

        return require({Jobs::FETCH_FROM_STACK,
        {
            {"item_type_id", mItemTypeId},
            {"from_item_stack_id", mItemStackId},
        }});
    }

    //I should now have a container to try. I must then put the item in it
    return require({Jobs::PUT_ITEM_IN_CONTAINER,
    {
        {"item_type_id", mItemTypeId},
        {"to_container_stack_id", mContainerId},
    }});
}

void FromStackToContainerSolver::notifySuccess(AnyMap<std::string> completedData)
{
    if(mRequiredJob.type == Jobs::FETCH_FROM_STACK)
    {//I now have the item
        mFetchedItem = true;
    }
    else if(mRequiredJob.type == Jobs::PUT_ITEM_IN_CONTAINER)
    {
        mPutItem = true;
    }
}

void FromStackToContainerSolver::notifyFail(AnyMap<std::string> failedData)
{
    if(mRequiredJob.type == Jobs::FETCH_FROM_STACK || mRequiredJob.type == Jobs::PUT_ITEM_IN_CONTAINER)
    {//I can't fetch or put the item. I'm screwed
        mCannotFetch = true;
    }
}
