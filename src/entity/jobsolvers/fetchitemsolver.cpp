#include "entity/jobsolvers/fetchitemsolver.hpp"
#include "world/gameworld.hpp"
#include "world/tiles.hpp"
#include "entity/entitymessages.hpp"
#include "world/landscapesemantics.hpp"
#include "items/inventory.hpp"
#include "util/log.hpp"
#include "facility/jobs.hpp"

FetchItemSolver::FetchItemSolver(const Job& job, const AnyMap<std::string>& external):
    JobSolver::JobSolver(job, external),
    mBus(external.at("message_bus").get<std::reference_wrapper<fea::MessageBus>>()),
    mItemTypeId(job.jobData.at("item_type_id").get<int32_t>())
{
    auto it = job.jobData.find("from_item_stack_id");
    if(it != job.jobData.end())
    {
        mIsStack = true;
        mStackId = it->second.get<int32_t>();
    }
    else if((it = job.jobData.find("from_container_stack_id")) != job.jobData.end())
    {
        mIsContainer = true;
        mStackId = it->second.get<int32_t>();
    }
}

JobSolver::UpdateResult FetchItemSolver::update()
{

    if(mFailed)
        return fail();

    if(mSucceed)
        return succeed();

    if(mIsStack)
    {
        return require({Jobs::FETCH_FROM_STACK,
        {
            {"item_type_id", mItemTypeId},
            {"from_item_stack_id", mStackId}
        }});
    }
    else if(mIsContainer)
    {
        return require({Jobs::FETCH_FROM_CONTAINER,
        {
            {"item_type_id", mItemTypeId},
            {"from_container_stack_id", mStackId}
        }});
    }
    else
    {
        FEA_ASSERT(false, "Fetching item not on stack or on container");
        return fail();
    }

    return working();
}

void FetchItemSolver::notifySuccess(AnyMap<std::string> completedData)
{
    mFailed = true;
}

void FetchItemSolver::notifyFail(AnyMap<std::string> failedData)
{
    mSucceed = true;
}
