#pragma once
#include "jobai/jobsolver.hpp"
#include "entity/carrycontroller.hpp"
#include "world/landscape.hpp"
#include "world/itemmanager.hpp"

class GameWorld;

class MineWallSolver : public JobSolver
{
    public:
        MineWallSolver(const Job& job, const AnyMap<std::string>& external);
        UpdateResult update() override;
        void notifySuccess(AnyMap<std::string> completedData) override;
        void notifyFail(AnyMap<std::string> failedData) override;
    private:
        fea::MessageBus& mBus;
        GameWorld& mWorld;
        fea::EntityPtr mEntity;

        glm::ivec2 mTargetTile;
};
