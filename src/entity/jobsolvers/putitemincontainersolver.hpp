#pragma once
#include "jobai/jobsolver.hpp"
#include "entity/carrycontroller.hpp"
#include "entity/iteminvokercontroller.hpp"
#include "world/itemmanager.hpp"

class GameWorld;

class PutItemInContainerSolver : public JobSolver
{
    public:
        PutItemInContainerSolver(const Job& job, const AnyMap<std::string>& external);
        UpdateResult update() override;
        void notifySuccess(AnyMap<std::string> completedData) override;
        void notifyFail(AnyMap<std::string> failedData) override;
    private:
        void validateStack(th::Optional<ItemManager::ItemStackView>& itemStack) const;
        fea::MessageBus& mBus;
        GameWorld& mWorld;
        CarryController& mCarryController;
        ItemInvokerController& mItemInvokerController;
        fea::EntityPtr mEntity;

        int32_t mItemTypeId;
        const ItemType& mItemType;
        int32_t mContainerStackId;
        th::Optional<ItemManager::ItemStackView> mTargetContainer;
};
