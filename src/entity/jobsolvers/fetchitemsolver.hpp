#pragma once
#include "jobai/jobsolver.hpp"
#include "entity/carrycontroller.hpp"
#include "world/itemmanager.hpp"

class GameWorld;

class FetchItemSolver : public JobSolver
{
    public:
        FetchItemSolver(const Job& job, const AnyMap<std::string>& external);
        UpdateResult update() override;
        void notifySuccess(AnyMap<std::string> completedData) override;
        void notifyFail(AnyMap<std::string> failedData) override;
    private:
        fea::MessageBus& mBus;

        bool mIsContainer = false;
        bool mIsStack = false;

        bool mFailed = false;
        bool mSucceed = false;

        int32_t mStackId;
        int32_t mItemTypeId;
};
