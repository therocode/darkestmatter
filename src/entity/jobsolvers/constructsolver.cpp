#include "constructsolver.hpp"
#include "world/gameworld.hpp"
#include "world/tiles.hpp"
#include "items/inventory.hpp"
#include "entity/evadetileset.hpp"
#include "world/constructionmanager.hpp"
#include "facility/jobs.hpp"
#include "world/itemmanager.hpp"
#include "entity/carrycontroller.hpp"

ConstructSolver::ConstructSolver(const Job& job, const AnyMap<std::string>& external):
    JobSolver::JobSolver(job, external),
    mBus(external.at("message_bus").get<std::reference_wrapper<fea::MessageBus>>()),
    mWorld(external.at("world").get<std::reference_wrapper<GameWorld>>()),
    mEntity(external.at("entity").get<fea::EntityPtr>()),
    mCarryController(external.at("carry_controller").get<std::reference_wrapper<CarryController>>()),
    mTargetPosition(job.jobData.at("position").get<glm::ivec2>()),
    mTargetType(job.jobData.at("type").get<ConstructionType>()),
    mCannotReachSite(false),
    mCannotFindMaterial(false),
    mCannotClearTiles(false),
    mSiteCleared(false)
{
}

JobSolver::UpdateResult ConstructSolver::update()
{
    glm::ivec2 position = Tiles::toTile(mEntity->getAttribute<glm::vec2>("position"));
    bool inRange = position == mTargetPosition;

    float acceptableDistance = glm::length((glm::vec2)Tiles::size())/4.f;

    //possible fail conditions
    if(mCannotReachSite || mCannotFindMaterial || mCannotClearTiles)
        return fail();

    //performing
    if(!mSite)
    {//I don't have a construction site to work on, I better see if one exists

        auto existingSite = mWorld.constructions().at(mTargetPosition);

        if(!existingSite)
        {//there is no site! I'll create one
            if(inRange)
            {//I am in range of the spot, so I can just create it!
                mSite = mWorld.constructions().create(mTargetType, mTargetPosition);

                if(!mSite)
                {//I couldn't create one. I must fail
                    return fail();
                }
            }
            else
            {//I am not at the planned site, I better go there
                return require({Jobs::GOTO, {{"position", Tiles::tileCenter(mTargetPosition)}, {"minimumDistance", acceptableDistance}}});
            }
        }
        else
        {//there is a current one. Is it of the right type and such?
            const auto& existingConstruction = mWorld.constructions().get(*existingSite);

            if(existingConstruction->constructionType == mTargetType)
            {//it is! Now we can adopt this site
                mSite = existingSite;
            }
            else
            {//it is not. This means another construction is in the way of this one. That's a fail
                return fail();
            }
        }
    }

    //now I have a valid site
    const Construction* construction = mWorld.constructions().get(*mSite);

    if(construction)
    {//I have a site ready to be built!

        const auto& materialsNeeded = construction->materialsNeeded;
        if(materialsNeeded.empty())
        {//this construction does not need any materials. We can then proceed with building it
            //if this is the frame the construction will be finished, then I am allowed to finish it over distance. I should also make sure that no one is overlapping it before I do so
            //
            int32_t untilDone = construction->untilDone;
            int32_t constructionSpeed = mEntity->getAttribute<int32_t>("construction_speed");

            if(constructionSpeed >= untilDone)
            {//I will finish it this frame, so range doesn't matter and I will finalize it after I clear the tile
                if(!mSiteCleared)
                {//clear the tile
                    return require({Jobs::CLEAR_TILES, {{"target_tiles", EvadeTileSet{mTargetPosition}}}});
                }
                else
                {//site is clear, finish it!
                    bool done = mWorld.constructions().construct(*mSite, constructionSpeed) == 0;

                    if(done)
                    {//the structure is now finished, hooray!
                        return succeed();
                    }
                    else
                    {
                        FEA_ASSERT(false, "It didn't finish even though it should have. Bug");
                    }
                }
            }
            else
            {//I will not finish it this frame, so I can construct normally and I have to be in range
                if(inRange)
                {//I am in range! let's build it now using my construction speed
                    bool done = mWorld.constructions().construct(*mSite, constructionSpeed) == 0;

                    if(done)
                    {
                        FEA_ASSERT(false, "It did finish even though it shouldn't have. Bug");
                    }
                }
                else
                {//I am not in range of the construction. This must be fixed
                    return require({Jobs::GOTO, {{"position", Tiles::tileCenter(mTargetPosition)}, {"minimumDistance", acceptableDistance}}});
                }
            }
        }
        else
        {//this construction needs materials! I must find them if possible

            int32_t neededId = materialsNeeded.begin()->first;

            auto material = mCarryController.takeItem(mEntity->getId(), neededId); //take out the needed material from the inventory

            if(material)
            {//I have a needed item. Apply it
                mWorld.constructions().addMaterial(*mSite, std::move(*material));
            }
            else
            {//let's call a fetch job on the first needed material and take it from there
                return require({Jobs::FIND_AND_FETCH, {{"item_type_id", materialsNeeded.begin()->first}}});
            }
        }
    }
    else
    {//the construction is gone or broken or something! I better fail
        return fail();
    }

    return working();
}

void ConstructSolver::notifySuccess(AnyMap<std::string> completedData)
{
    if(mRequiredJob.type == Jobs::CLEAR_TILES)
    {
        mSiteCleared = true;
    }
}

void ConstructSolver::notifyFail(AnyMap<std::string> failedData)
{
    if(mRequiredJob.type == Jobs::GOTO)
    {
        glm::vec2 target = mRequiredJob.jobData.at("position").get<glm::vec2>();

        if(Tiles::toTile(target) == mTargetPosition)
        {//Seems like I can't reach the site location. Nothing I can do then
            LOG_V("Entity " << mEntity->getId() << " here, I am trying to build but I can't reach my target site. Hence I'll fail :(");
            mCannotReachSite = true;
        }
    }
    else if(mRequiredJob.type == Jobs::FIND_AND_FETCH)
    {
        int32_t itemId = mRequiredJob.jobData.at("item_type_id").get<int32_t>();

        //Seems like I can't get the material. I'll fail
        LOG_V("Entity " << mEntity->getId() << " here, I am trying to build but I can't get material of id " << itemId << ". Hence I'll fail :(");
        mCannotFindMaterial = true;
    }
    else if(mRequiredJob.type == Jobs::CLEAR_TILES)
    {
        mCannotClearTiles = true;
    }
}
