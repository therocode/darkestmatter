#include "workercontroller.hpp"
#include "facility/facility.hpp"
#include "facility/jobcategory.hpp"
#include "util/selectrandom.hpp"
#include "entity/taskaicontroller.hpp"
#include "workerstatus.hpp"

WorkerController::WorkerController(fea::MessageBus& bus, Facility& facility, TaskAIController& taskAIController):
    mBus(bus),
    mFacility(facility),
    mTaskAIController(taskAIController)
{
    subscribe(mBus, *this);
}

bool WorkerController::keepEntity(fea::EntityPtr entity) const
{
    bool keep = entity->hasAttribute("work_status");

    return keep;
}

void WorkerController::entityKept(fea::EntityPtr entity)
{
}

void WorkerController::entityDestroyed(fea::EntityPtr entity)
{
}

void WorkerController::update(float deltaTime)
{
    for(auto cooldownMapIter = mFailCooldowns.begin(); cooldownMapIter != mFailCooldowns.end();)
    {
        auto& cooldownMap = cooldownMapIter->second;
        for(auto failedIter = cooldownMap.begin(); failedIter != cooldownMap.end();)
        {
            --failedIter->second;

            if(failedIter->second <= 0)
                failedIter = cooldownMap.erase(failedIter);
            else
                ++failedIter;
        }

        if(cooldownMap.empty())
            cooldownMapIter = mFailCooldowns.erase(cooldownMapIter);
        else
            ++cooldownMapIter;
    }

    for(auto entityIter : mEntities)
    {
        fea::EntityPtr entity = entityIter.second;
        auto& workStatus = entity->getAttribute<WorkerStatus>("work_status");

        if(workStatus == WorkerStatus::IDLE)
        {
            auto jobList = mFacility.jobs().find(true);

            if(!jobList.empty())
            {//in lack of a priority system, prioritise jobs over routines
                const auto& cooldownMap = mFailCooldowns[entity->getId()];

                for(const auto& jobEntry : jobList)
                {//cycle jobs until a fitting one is found
                    if(cooldownMap.count(jobEntry.id) == 0)
                    {
                        const Job& job = jobEntry.job;
                        FEA_ASSERT(job.jobData.count("id") != 0, "Job lacks id");

                        LOG_I("I am entity '" << entity->getId() << "' and I got '" << job.type << "'job at '" << job.jobData.at("position").get<glm::ivec2>() << "\n");

                        auto jobId = mTaskAIController.doJob(entity->getId(), job);

                        if(jobId)
                        {
                            mFacility.jobs().update(jobEntry.id, WorkStatus::IN_PROGRESS);
                            workStatus = WorkerStatus::WORKING;
                            mNowPerforming.emplace(*jobId, entity->getId());
                        }

                        break;
                    }
                }
            }
            else
            {//there are no jobs, check if there are routines
                auto routineList = mFacility.routines().find(true);

                if(!routineList.empty())
                {//there are routines!

                    const auto& selected = selectRandom(routineList.begin(), routineList.end()); //no priority system, choose a random one 

                    const Job& job = selected->job;

                    auto jobId = mTaskAIController.doJob(entity->getId(), job);

                    if(jobId)
                    {//I am now solving the routine
                        workStatus = WorkerStatus::WORKING;
                        mNowPerforming.emplace(*jobId, entity->getId());
                    }
                }
            }
        }
        else
        {
        }
    }
}

void WorkerController::handleMessage(const JobFinishedMessage& message)
{
    auto performingIter = mNowPerforming.find(message.jobId);

    if(performingIter != mNowPerforming.end())
    {//this is a job I started.
        FEA_ASSERT(message.entity->getId() == performingIter->second, "Another entity finished the job this entity started in a weird way");
        const auto& job = message.job;
        FEA_ASSERT(job.jobData.count("job_category") != 0 && job.jobData.at("job_category").isOfType<JobCategory>(), "Job data 'job_category' not correctly added");
        FEA_ASSERT(job.jobData.count("id") != 0, "Job lacks id");

        JobCategory category = job.jobData.at("job_category").get<JobCategory>();

        if(category == JobCategory::JOB)
        {//only jobs needs to be updated - not routines
            mFacility.jobs().update(job.jobData.at("id").get<int32_t>(), WorkStatus::COMPLETED);
        }

        message.entity->setAttribute("work_status", WorkerStatus::IDLE);
        mNowPerforming.erase(performingIter);
    }
}

void WorkerController::handleMessage(const JobFailedMessage& message)
{
    auto performingIter = mNowPerforming.find(message.jobId);

    if(performingIter != mNowPerforming.end())
    {//this is a facility job I started. Must let facility know
        FEA_ASSERT(message.entity->getId() == performingIter->second, "Another entity finished the job this entity started in a weird way");
        const auto& job = message.job;
        FEA_ASSERT(job.jobData.count("job_category") != 0 && job.jobData.at("job_category").isOfType<JobCategory>(), "Job data 'job_category' not correctly added");
        FEA_ASSERT(job.jobData.count("id") != 0, "Job lacks id");

        JobCategory category = job.jobData.at("job_category").get<JobCategory>();

        if(category == JobCategory::JOB)
        {//only jobs needs updating - not routines. also, routines don't have cooldown right now
            mFacility.jobs().update(job.jobData.at("id").get<int32_t>(), WorkStatus::WAITING);
            mFailCooldowns[message.entity->getId()].emplace(job.jobData.at("id").get<int32_t>(), 600 + rand() % 100); //only retry after 10 seconds (with arbitrary variation)
        }

        message.entity->setAttribute("work_status", WorkerStatus::IDLE);
        mNowPerforming.erase(performingIter);
    }
}
