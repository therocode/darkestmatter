#pragma once
#include "world/worldmessages.hpp"
#include <fea/entitysystem.hpp>
#include <fea/util.hpp>

class AnimatedSpriteController: public fea::EntityController, public fea::MessageReceiver<EntityMovedMessage>
{
    public:
        AnimatedSpriteController(fea::MessageBus& bus);
        virtual bool keepEntity(fea::EntityPtr entity) const override;
        virtual void entityKept(fea::EntityPtr entity) override;
        virtual void entityDestroyed(fea::EntityPtr entity) override;
        virtual void handleMessage(const EntityMovedMessage& message) override;
    private:
        fea::MessageBus& mBus;
};
