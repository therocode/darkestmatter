#pragma once
#include "jobai/brain.hpp"
#include "entity/braintype.hpp"
#include <util/anymap.hpp>

class BrainFactory
{
    public:
        static Brain generate(BrainType type, AnyMap<std::string> external);
};
