#pragma once
#include "world/landscapecollisionchecker.hpp"

class PhysicsController: public fea::EntityController
{
    public:
        PhysicsController(fea::MessageBus& bus, const Landscape& landscape);
        virtual bool keepEntity(fea::EntityPtr entity) const override;
        virtual void entityKept(fea::EntityPtr entity) override;
        virtual void entityDestroyed(fea::EntityPtr entity) override;
        virtual void update(float deltaTime) override;
    private:
        fea::MessageBus& mBus;
        LandscapeCollisionChecker mLandscapeCollisionChecker;
};
