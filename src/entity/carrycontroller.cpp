#include "carrycontroller.hpp"
#include "items/inventory.hpp"
#include "world/gameworld.hpp"
#include "world/tiles.hpp"

CarryController::CarryController(fea::MessageBus& bus, GameWorld& world):
    mBus(bus),
    mWorld(world)
{
    (void)mBus;
    (void)mWorld;
}

bool CarryController::keepEntity(fea::EntityPtr entity) const
{
    bool keep = entity->hasAttribute("max_carry_weight") &&
                entity->hasAttribute("max_carry_amount") &&
                entity->hasAttribute("inventory");

    return keep;
}

void CarryController::entityKept(fea::EntityPtr entity)
{
    int32_t maxAmount = entity->getAttribute<int32_t>("max_carry_amount");
    int64_t maxWeight = entity->getAttribute<int64_t>("max_carry_weight");

    entity->setAttribute("inventory", Inventory({maxAmount}, {maxWeight}));
}

void CarryController::entityDestroyed(fea::EntityPtr entity)
{
}

void CarryController::update(float deltaTime)
{
    for(auto entityIter : mEntities)
    {
    }
}

th::Optional<int32_t> CarryController::inRange(fea::EntityId id) const
{
    FEA_ASSERT(mEntities.count(id) != 0, "I was given bad entity id " << id);

    fea::EntityPtr entity = mEntities.at(id);
    const glm::vec2& position = entity->getAttribute<glm::vec2>("position");

    auto stack = mWorld.items().stackAt(HalfTiles::toTile(position));

    if(stack)
        return stack->stack.get().type.typeId;
    else
        return {};
}

bool CarryController::canPickUp(fea::EntityId id, const ItemType& itemType) const
{
    FEA_ASSERT(mEntities.count(id) != 0, "I was given bad entity id " << id);

    fea::EntityPtr entity = mEntities.at(id);
    Inventory& inventory = entity->getAttribute<Inventory>("inventory");

    return inventory.canFit(itemType);
}

bool CarryController::pickupItem(fea::EntityId id, int32_t itemTypeId)
{
    FEA_ASSERT(mEntities.count(id) != 0, "I was given bad entity id " << id);

    fea::EntityPtr entity = mEntities.at(id);
    const glm::vec2& position = entity->getAttribute<glm::vec2>("position");
    Inventory& inventory = entity->getAttribute<Inventory>("inventory");

    auto item = mWorld.items().takeItem(HalfTiles::toTile(position), 0); //right now just grab the first item from the stack

    if(item)
    {
        if(inventory.canFit(item->type()))
        {//we can fit it, so store it
            inventory.insert(std::move(*item));
            return true;
        }
        else
        {//we can't fit it, so return it to the world
            mWorld.items().putItem(std::move(*item), HalfTiles::toTile(position));
            return false;
        }
    }

    return false;
}

bool CarryController::dropItem(fea::EntityId id, int32_t itemTypeId)
{
    FEA_ASSERT(mEntities.count(id) != 0, "I was given bad entity id " << id);

    fea::EntityPtr entity = mEntities.at(id);
    const glm::vec2& position = entity->getAttribute<glm::vec2>("position");
    Inventory& inventory = entity->getAttribute<Inventory>("inventory");

    if(!inventory.hasItem(itemTypeId)) //what de hell? we dont have the item to drop lol
        return false;

    auto item = inventory.takeItem(itemTypeId);
    if(!item)//could not take item outs
        return false;

    item = mWorld.items().putItem(*item, HalfTiles::toTile(position));

    if(item)
    {//oh no we could not put the item

        //let's try to put it back
        if((item = giveItem(id, *item)))
        {//failed to put it back, let's crash
            FEA_ASSERT(false, "i have an item and i dont know what to do whit it -CarryController::dropItem");
            return false;
        }
    }

    return true;
}

th::Optional<Item> CarryController::giveItem(fea::EntityId id, Item item)
{
    FEA_ASSERT(mEntities.count(id) != 0, "I was given bad entity id " << id);

    fea::EntityPtr entity = mEntities.at(id);
    Inventory& inventory = entity->getAttribute<Inventory>("inventory");

    return inventory.insert(std::move(item));
}

th::Optional<Item> CarryController::takeItem(fea::EntityId id, int32_t itemTypeId)
{
    FEA_ASSERT(mEntities.count(id) != 0, "I was given bad entity id " << id);

    fea::EntityPtr entity = mEntities.at(id);
    Inventory& inventory = entity->getAttribute<Inventory>("inventory");

    return inventory.takeItem(itemTypeId);
}

bool CarryController::hasItem(fea::EntityId id, int32_t itemTypeId) const
{

    fea::EntityPtr entity = mEntities.at(id);
    Inventory& inventory = entity->getAttribute<Inventory>("inventory");

    return inventory.hasItem(itemTypeId);
}

const Inventory& CarryController::inventory(fea::EntityId id)
{
    FEA_ASSERT(mEntities.count(id) != 0, "I was given bad entity id " << id);

    fea::EntityPtr entity = mEntities.at(id);
    return entity->getAttribute<Inventory>("inventory");
}
