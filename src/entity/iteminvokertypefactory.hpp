#pragma once
#include <fea/entitysystem.hpp>
#include <thero/polymorphicwrapper.hpp>
#include "items/iteminvoker.hpp"
#include "entity/iteminvokertype.hpp"

using EntityItemInvoker = th::PolymorphicWrapper<ItemInvoker>;

class ItemInvokerTypeFactory
{
    public:
        static EntityItemInvoker generate(fea::EntityPtr entity, ItemInvokerType type);
};
