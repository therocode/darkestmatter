#pragma once
#include <thero/smartenum.hpp>

smart_enum_class(MovementAction, IDLE, WALK, RUN)
