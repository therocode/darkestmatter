#pragma once
#include <deque>
#include <unordered_map>
#include <util/numberpool.hpp>
#include "jobai/job.hpp"
#include "routinestatus.hpp"

class RoutineBoardCallbackHandler;

class RoutineBoard
{
    public:
        struct JobView
        {
            int32_t id;
            const Job& job;
            RoutineStatus status;
        };

        struct JobEntry
        {
            std::unique_ptr<Job> job; //need unique_ptr for it to never be invalidated
            RoutineStatus status;
        };

        RoutineBoard(RoutineBoardCallbackHandler* callbackHandler = nullptr);
        int32_t add(Job job);
        void update(int32_t id, RoutineStatus newStatus);
        std::deque<JobView> find(bool onlyEnabled) const;
    private:
        RoutineBoardCallbackHandler* mCallbackHandler;
        std::unordered_map<int32_t, JobEntry> mJobs;
        NumberPool<int32_t> mJobIdPool;
};
