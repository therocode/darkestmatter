#pragma once
#include "jobai/job.hpp"
#include "workstatus.hpp"

class JobBoardCallbackHandler;

class JobBoard
{
    public:
        struct JobView
        {
            int32_t id;
            const Job& job;
            WorkStatus status;
        };

        struct JobEntry
        {
            std::unique_ptr<Job> job; //need unique ptr for it to never be invalidated
            WorkStatus status;
        };

        JobBoard(JobBoardCallbackHandler* callbackHandler = nullptr);
        int32_t add(Job job);
        void update(int32_t id, WorkStatus newStatus);
        template<typename ParameterType>
        std::deque<JobView> find(int32_t type, const std::string& parameter, const ParameterType& value, bool onlyWaiting = true) const;
        std::deque<JobView> find(int32_t type, bool onlyWaiting = true) const;
        std::deque<JobView> find(bool onlyWaiting = true) const;
    private:
        std::unordered_map<int32_t, std::deque<int32_t>> mJobIndex;
        std::unordered_map<int32_t, JobEntry> mJobs;
        NumberPool<int32_t> mJobIdPool;
        JobBoardCallbackHandler* mCallbackHandler;
};


template<typename ParameterType>
std::deque<JobBoard::JobView> JobBoard::find(int32_t type, const std::string& parameter, const ParameterType& value, bool onlyWaiting) const
{
    std::deque<JobView> result;

    const auto& jobList = mJobIndex.find(type);

    if(jobList != mJobIndex.end())
    {
        for(const auto& jobId : jobList->second)
        {
            const auto& jobEntry = mJobs.at(jobId);

            if(onlyWaiting ? (jobEntry.status == WorkStatus::WAITING) : true) // only waiting jobs if true
            {
                auto parameterIterator = jobEntry.job->jobData.find(parameter);

                if(parameterIterator != jobEntry.job->jobData.end()) //does it exist?
                {
                    if(parameterIterator->second.isOfType<ParameterType>()) //is it of the right type?
                    {
                        if(parameterIterator->second.get<ParameterType>() == value) //does it equal this value?
                        {
                            result.emplace_back(JobView{jobId, *jobEntry.job, jobEntry.status});
                        }
                    }
                }
            }
        }
    }

    return result;
}
