#include "featextureadaptor.hpp"
#include <iostream>
        
FeaTextureAdaptor::FeaTextureAdaptor(fea::Texture& texture):
        mTexture(texture)
{
}   
        
void FeaTextureAdaptor::initialize(uint32_t width, uint32_t height)
{       
    mTexture.create({width, height}, fea::Color(0, 0, 0, 0), false); 
    std::cout << "initialized with " << width << " " << height << " and I am id " << mTexture.getId() << "\n";
}   
        
void FeaTextureAdaptor::resize(uint32_t width, uint32_t height)
{
    mTexture.resize({width, height});
    std::cout << "resized with " << width << " " << height << " and I am id " << mTexture.getId() << "\n";
}

void FeaTextureAdaptor::writeBitmap(uint32_t xPos, uint32_t yPos, const gim::BitMap& bitMap)
{
    for(uint32_t x = 0; x < bitMap.width; x++)
    {
        for(uint32_t y = 0; y < bitMap.height; y++)
        {
            uint8_t value = bitMap.pixels[x + y * bitMap.width];

            if(value != 0)
                mTexture.setPixel({xPos + x, yPos + y}, fea::Color(value, value, value, value));
        }
    }

    mTexture.update();
}

glm::ivec2 FeaTextureAdaptor::size() const
{
    return mTexture.getSize();
}

uint32_t FeaTextureAdaptor::handle() const
{
    return mTexture.getId();
}
