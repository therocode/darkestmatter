#include "uievents.hpp"
#include <thero/optional.hpp>
#include <fea/render2d.hpp>

class UiTools
{
    public: 
        static void addCallback(gim::Element& element, const std::string& name, Callback callback);
        static void toggleVisibility(gim::Element& element);
        static void toggleChildrenVisibility(gim::Element& element);
        template<typename ValueType>
        static th::Optional<ValueType> getOrNone(const gim::Element& element, const std::string& name)
        {
            const auto* value = element.findAttribute<ValueType>(name);

            if(value)
                return {*value};
            else
                return {};
        }
        static void setOnHoverColor(gim::Element& element, const Parameters& params);
        static void setDefaultColor(gim::Element& element, const Parameters& params);
};
