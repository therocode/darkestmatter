#pragma once
#include <cstdint>
#include <glm/glm.hpp>

class Vec2Adaptor
{
    public:
        using Native = glm::ivec2;
        Vec2Adaptor();
        Vec2Adaptor(int32_t x, int32_t y); 
        Vec2Adaptor(const glm::ivec2& vec2);
        Vec2Adaptor operator+(const Vec2Adaptor& other) const;
        int32_t x() const;
        int32_t y() const;
        void x(int32_t x); 
        void y(int32_t y); 
    private:
        glm::ivec2 mVec2;
};

