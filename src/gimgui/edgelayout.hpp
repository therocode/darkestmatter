#include "precompiled.h"

struct EdgeLayout
{
    enum Edge { TOP, LEFT, RIGHT, BOTTOM };

    glm::ivec2 topLeftPadding;
    glm::ivec2 bottomRightPadding;
};


struct EdgeLayoutData
{
    EdgeLayout::Edge edge;         //which edge to be attached to
    int32_t position;  //position along the edge where 0 is one end and 1000 is the other
    th::Optional<int32_t> size;      //size along the edge where 1000 is the whole size of the edge
    glm::ivec2 offset; //final pixel offset to apply
};
