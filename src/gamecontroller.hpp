#pragma once
#include "messages.hpp"
#include "world/worldmessages.hpp"
#include "facility/facilitycontroller.hpp"

class Facility;
class GameWorld;
class ResourceStorage;
class GameUI;

smart_enum_class(PositionAction, PRIMARY, SECONDARY, TERTIARY)

class GameController: public FacilityController, public fea::MessageReceiver<
    ConstructionCreatedMessage
    >
{
    public:
        GameController(fea::MessageBus& bus, Facility& facility, GameWorld& gameWorld, const ResourceStorage& resources);
        void positionAction(const glm::vec2& position, PositionAction action);
        void handleMessage(const ConstructionCreatedMessage& message) override;
        void jobCreated(int32_t id, const Job& job) override;
        void jobUpdated(int32_t id, WorkStatus status, const Job& job) override;
        void setUI(GameUI& gameUI);
    private:
        void newConstructionOrder(const glm::ivec2& position, ConstructionType type);
        void cancelWallOrder(const glm::ivec2& position);
        void newMineOrder(const glm::ivec2& position);
        void cancelMineOrder(const glm::ivec2& position);
        void createRoom(const glm::ivec2& position);
        fea::MessageBus& mBus;
        Facility& mFacility;
        GameWorld& mWorld;
        const ResourceStorage& mResources;
        GameUI* mGameUI;
};
