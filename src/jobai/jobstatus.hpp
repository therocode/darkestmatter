#pragma once
#include <thero/smartenum.hpp>

smart_enum_class(JobStatus, IDLE, WORKING, COMPLETED, FAILED, REQUIRES)
