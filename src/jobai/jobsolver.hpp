#pragma once
#include "job.hpp"
#include "jobstatus.hpp"

class JobSolver
{
    public:
        struct UpdateResult
        {
            JobStatus status;
            AnyMap<std::string> output;
            const Job* requiredJob = nullptr;
        };
        JobSolver(const Job& job, const AnyMap<std::string>& external);
        virtual ~JobSolver() = default;
        const Job& job();
        virtual UpdateResult update() = 0;
        virtual void notifySuccess(AnyMap<std::string> completedData);
        virtual void notifyFail(AnyMap<std::string> failedData);
    protected:
        UpdateResult fail(AnyMap<std::string> output = AnyMap<std::string>());
        UpdateResult succeed(AnyMap<std::string> output = AnyMap<std::string>());
        UpdateResult require(Job job);
        UpdateResult working(AnyMap<std::string> output = AnyMap<std::string>());
        const Job& mJobToSolve;
        const AnyMap<std::string>& mExternal;
        Job mRequiredJob;
};
