#pragma once
#include "resources/resourcestorage.hpp"
#include "gameinputhandler.hpp"

class RenderingSystem;
class GameWorld;
class GameController;
class GameUI;

class GameSession: public fea::MessageReceiver<TogglePauseMessage, ResizeMessage>
{
    public:
        GameSession(fea::MessageBus& bus, fea::InputHandler& inputHandler);
        ~GameSession();
        void update();
        void handleMessage(const TogglePauseMessage& message) override;
        void handleMessage(const ResizeMessage& message) override;
    private:
        fea::MessageBus& mBus;
        ResourceStorage mResources;
        GameInputHandler mInputHandler;
        Pimpl<RenderingSystem> mRenderingSystem;
        std::unique_ptr<GameWorld> mGameWorld;
        std::unique_ptr<GameController> mGameController;
        std::unique_ptr<GameUI> mGameUI;

        bool mPaused;
};
