#include "constructionmanager.hpp"
#include "world/landscapesemantics.hpp"
#include "constructionmanagercallbackhandler.hpp"

ConstructionManager::ConstructionManager(ConstructionManagerCallbackHandler& constructionManagerCallbackHandler, Landscape& landscape):
    mConstructionManagerCallbackHandler(constructionManagerCallbackHandler),
    mLandscape(landscape)
{
}

void ConstructionManager::addConstructionType(ConstructionType type, ConstructionTypeData typeData)
{
    FEA_ASSERT(mTypes.count(type) == 0, "Cannot add type '" << type.category << "," << type.type << "' since it is already added");
    FEA_ASSERT(type.category == ConstructionCategory::WALL ? typeData.size == glm::ivec2(1, 1) : 1, "Walls must be 1x1 in size, not " << typeData.size);
    FEA_ASSERT(type.category == ConstructionCategory::FLOOR ? typeData.size == glm::ivec2(1, 1) : 1, "Floors must be 1x1 in size, not " << typeData.size);

    mTypes.emplace(type, std::move(typeData));
}

th::Optional<int32_t> ConstructionManager::create(ConstructionType type, const glm::ivec2& position)
{
    if(constructionAllowed(position))
    {
        FEA_ASSERT(mTypes.count(type) != 0, "Cannot construct type '" << type.category << "," << type.type << "' since it doesn't exist");

        const auto& constructionType = mTypes.at(type);

        int32_t newId = mConstructionIdPool.next();

        Construction newConstruction;

        if(type.category == ConstructionCategory::WALL)
        {
            newConstruction = 
            {
                type,
                constructionType.constructionAmount,
                position,
                {position},
                constructionType.materialsNeeded,
                {}, //has no items added yet
            };
        }
        else if(type.category == ConstructionCategory::FLOOR)
        {
            newConstruction = 
            {
                type,
                constructionType.constructionAmount,
                position,
                {position},
                constructionType.materialsNeeded,
                {}, //has no items added yet
            };
        }
        else
        {
            FEA_ASSERT(false, "Error, unimplemented/unsupported category " << type.category);
        }

        for(const auto& occupiedPosition : newConstruction.occupiedPositions)
            mPositionIndex.emplace(occupiedPosition, newId);

        mConstructions.emplace(newId, std::make_unique<Construction>(std::move(newConstruction)));

        mConstructionManagerCallbackHandler.constructionCreated(type, position, newId);

        return newId;
    }
    else
        return {};
}

th::Optional<int32_t> ConstructionManager::at(const glm::ivec2& position) const
{
    auto constructionIterator = mPositionIndex.find(position);

    if(constructionIterator != mPositionIndex.end())
    {
        return constructionIterator->second;
    }
    else
        return {};
}

const Construction* ConstructionManager::get(int32_t id) const
{
    auto constructionIterator = mConstructions.find(id);

    if(constructionIterator != mConstructions.end())
        return constructionIterator->second.get();
    else
        return nullptr;
}

bool ConstructionManager::constructionAllowed(const glm::ivec2& position) const
{
    return mPositionIndex.count(position) == 0 && //other constructions
        mLandscape.inBounds(position) && //world
        !LandscapeSemantics::isSolid(mLandscape.walls().at(position)); //world
}

int32_t ConstructionManager::construct(int32_t id, int32_t constructionAmount)
{
    FEA_ASSERT(mConstructions.count(id) != 0, "Cannot construct non-existing construction " << id);

    Construction& construction = *mConstructions.at(id);

    if(construction.materialsNeeded.empty())
    {//only construct if no additional materials are needed
        construction.untilDone -= constructionAmount;

        if(construction.untilDone <= 0)
        {
            //finalise
            if(construction.constructionType.category == ConstructionCategory::WALL)
            {
                //set wall tiles
                mLandscape.setWall(construction.position, construction.constructionType.type);   
            }
            else if(construction.constructionType.category == ConstructionCategory::FLOOR)
            {
                //set floor tiles
                mLandscape.setFloor(construction.position, construction.constructionType.type);
            }
            else
            {
                FEA_ASSERT(false, "Error, unimplemented/unsupported type " << construction.constructionType.category);
            }

            //erase
            for(const auto& tile : construction.occupiedPositions)
            {
                FEA_ASSERT(mPositionIndex.count(tile) != 0, "Removing a construction occupied tile, but it is missing!");
                mPositionIndex.erase(tile);
            }

            mConstructions.erase(id);
            mConstructionIdPool.release(id);

            mConstructionManagerCallbackHandler.constructionFinished(id);

            return 0;
        }
    }

    return construction.untilDone;
}

th::Optional<Item> ConstructionManager::addMaterial(int32_t id, Item item)
{
    FEA_ASSERT(mConstructions.count(id) != 0, "Cannot add material to non-existing construction " << id);

    auto& construction = mConstructions.at(id);

    int32_t materialId = item.type().typeId;
    auto requirementIter = construction->materialsNeeded.find(materialId);

    if(requirementIter != construction->materialsNeeded.end())
    {//I actually need the item that was given. Nice. Remove it from the need and store it
        construction->materialsNeeded.erase(materialId);
        construction->materialsAdded.push_back(std::move(item));
        return {};
    }
    else
    {//I didn't need the given item. Return it.
        return item;
    }
}
