#pragma once

#include <memory>
#include <unordered_set>
#include "world/constructiontype.hpp"

class Landscape;
struct Job;

using WallType = int32_t;
using FloorType = int32_t;
using GroundType = int32_t;

namespace fea
{
    class Entity;
    using EntityPtr = std::shared_ptr<Entity>;
}

//landscape
struct LandscapeLoadedMessage
{
    const Landscape& landscape;
};

struct WallTileSetMessage
{
    const glm::ivec2& position;
    WallType type;
};

struct GroundTileSetMessage
{
    const glm::ivec2& position;
    GroundType type;
};

struct FloorTileSetMessage
{
    const glm::ivec2& position;
    FloorType type;
};

//AnimatedSpriteController
struct AnimatedSpriteCreatedMessage
{
    int32_t entityId;
    const glm::vec2& position;
    int32_t entityGraphicsType;
};
struct AnimatedSpriteMovedMessage { int32_t entityId;
    const glm::vec2& position;
};

struct AnimatedSpriteDestroyedMessage
{
    int32_t entityId;
};

//PhysicsController
struct EntityMovedMessage
{
    int32_t entityId;
    const glm::vec2& position;
    const glm::vec2& oldPosition;
};

struct LandscapeCollisionMessage
{
    int32_t entityId;
    const std::unordered_set<glm::ivec2>& collidedTiles;
    const glm::vec2& impactVelocity;
};

//TaskAIController
struct JobFinishedMessage
{
    int32_t jobId;
    const Job& job;
    fea::EntityPtr entity;
};

struct JobFailedMessage
{
    int32_t jobId;
    const Job& job;
    fea::EntityPtr entity;
};

//Constructions
struct ConstructionCreatedMessage
{
    ConstructionType type;
    const glm::ivec2& position;
    int32_t id;
};

struct ConstructionFinishedMessage
{
    int32_t id;
};

//Items
struct ItemStackCreatedMessage
{
    int32_t itemType;
    int32_t itemStackId;
    const glm::vec2& position;
    int32_t stackAmount;
};

struct ItemStackUpdatedMessage
{
    int32_t itemStackId;
    int32_t stackAmount;
};

struct ItemStackDepletedMessage
{
    int32_t itemStackId;
};
