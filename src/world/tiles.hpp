#pragma once
#include <unordered_map>
#include <functional>
#include <glm/glm.hpp>
#include <unordered_set>
#include <fea/rendering/glmhash.hpp>
#include <vector>


class Tiles
{
    public:
        static glm::vec2 tileTopLeft(const glm::ivec2& tile);
        static glm::vec2 tileCenter(const glm::ivec2& tile);
        static glm::ivec2 toTile(const glm::vec2& position);
        static bool withinTile(const glm::vec2& position, const glm::ivec2& tile);
        static std::unordered_set<glm::ivec2> tilesToHalfTiles(const std::unordered_set<glm::ivec2>& tiles);
        static std::unordered_set<glm::ivec2> tilesToHalfTiles(const std::vector<glm::ivec2>& tiles);
        static const glm::ivec2& size();
};

class HalfTiles
{
    public:
        static glm::vec2 tileTopLeft(const glm::ivec2& tile);
        static glm::vec2 tileCenter(const glm::ivec2& tile);
        static glm::ivec2 toTile(const glm::vec2& position);
        static bool withinTile(const glm::vec2& position, const glm::ivec2& tile);
        static const glm::ivec2& size();
};
