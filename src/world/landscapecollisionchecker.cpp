#include "landscapecollisionchecker.hpp"
#include "world/landscapesemantics.hpp"

LandscapeCollisionChecker::LandscapeCollisionChecker(const Landscape& landscape):
    mLandscape(landscape)
{
}

float tileSize = 32.0f;

LandscapeCollisionChecker::CollisionResult LandscapeCollisionChecker::evaluate(const glm::vec2& oldPosition, const glm::vec2& newPosition, float width) const
{
    CollisionResult result;

    auto check = [&result, &width, this] (const glm::vec2& position)
    {
        std::unordered_set<glm::ivec2> collidedTiles;

        float halfWidth = width / 2.0f;

        glm::ivec2 tl = static_cast<glm::ivec2>((position + glm::vec2(-halfWidth, -halfWidth)) / 32.0f);
        glm::ivec2 tr = static_cast<glm::ivec2>((position + glm::vec2( halfWidth, -halfWidth)) / 32.0f);
        glm::ivec2 bl = static_cast<glm::ivec2>((position + glm::vec2(-halfWidth,  halfWidth)) / 32.0f);
        glm::ivec2 br = static_cast<glm::ivec2>((position + glm::vec2( halfWidth,  halfWidth)) / 32.0f);

        if(isSolidAt(tl))
            collidedTiles.emplace(tl);
        if(isSolidAt(tr))
            collidedTiles.emplace(tr);
        if(isSolidAt(bl))
            collidedTiles.emplace(bl);
        if(isSolidAt(br))
            collidedTiles.emplace(br);

        return collidedTiles;
    };

    auto position = oldPosition;
    
    //advance x
    position.x = newPosition.x;
    
    auto xColliders = check(position);

    if((result.xCollided = !xColliders.empty()))
    {
        result.collidedTiles.insert(xColliders.begin(), xColliders.end());

        //move back better
        position.x = oldPosition.x;
    }

    //advance y
    position.y = newPosition.y;

    auto yColliders = check(position);

    if((result.yCollided = !yColliders.empty()))
    {
        result.collidedTiles.insert(yColliders.begin(), yColliders.end());

        //move back better
        position.y = oldPosition.y;
    }

    result.position = position;

    return result;
}


bool LandscapeCollisionChecker::isSolidAt(const glm::ivec2& position) const
{
    if(mLandscape.inBounds(position))
        return LandscapeSemantics::isSolid(mLandscape.walls().at(position));
    return true;
}
