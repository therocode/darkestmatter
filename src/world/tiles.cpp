#include "tiles.hpp"

glm::vec2 Tiles::tileTopLeft(const glm::ivec2& tile)
{
    return static_cast<glm::vec2>(tile) * static_cast<glm::vec2>(size());
}

glm::vec2 Tiles::tileCenter(const glm::ivec2& tile)
{
    return static_cast<glm::vec2>(tile) * static_cast<glm::vec2>(size()) + static_cast<glm::vec2>(size()) / 2.0f;
}

glm::ivec2 Tiles::toTile(const glm::vec2& position)
{
    return static_cast<glm::ivec2>(position / static_cast<glm::vec2>(size()));
}

bool Tiles::withinTile(const glm::vec2& position, const glm::ivec2& tile)
{
    return toTile(position) == tile;
}

std::unordered_set<glm::ivec2> Tiles::tilesToHalfTiles(const std::unordered_set<glm::ivec2>& tiles)
{
    std::unordered_set<glm::ivec2> halfTiles;
    for(auto tile : tiles)
    {
        glm::ivec2 halfTilePos = HalfTiles::toTile(Tiles::tileTopLeft(tile));
        halfTiles.insert(halfTilePos);
        halfTiles.insert(halfTilePos + glm::ivec2(1, 0));
        halfTiles.insert(halfTilePos + glm::ivec2(0, 1));
        halfTiles.insert(halfTilePos + glm::ivec2(1, 1));
    }

    return halfTiles;
}

std::unordered_set<glm::ivec2> Tiles::tilesToHalfTiles(const std::vector<glm::ivec2>& tiles)
{
    std::unordered_set<glm::ivec2> halfTiles;
    for(auto tile : tiles)
    {
        glm::ivec2 halfTilePos = HalfTiles::toTile(Tiles::tileTopLeft(tile));
        halfTiles.insert(halfTilePos);
        halfTiles.insert(halfTilePos + glm::ivec2(1, 0));
        halfTiles.insert(halfTilePos + glm::ivec2(0, 1));
        halfTiles.insert(halfTilePos + glm::ivec2(1, 1));
    }

    return halfTiles;
}

const glm::ivec2& Tiles::size()
{
    static glm::ivec2 tileSize = {32, 32};

    return tileSize;
}

glm::vec2 HalfTiles::tileTopLeft(const glm::ivec2& tile)
{
    return static_cast<glm::vec2>(tile) * static_cast<glm::vec2>(size());
}

glm::vec2 HalfTiles::tileCenter(const glm::ivec2& tile)
{
    return static_cast<glm::vec2>(tile) * static_cast<glm::vec2>(size()) + static_cast<glm::vec2>(size()) / 2.0f;
}

glm::ivec2 HalfTiles::toTile(const glm::vec2& position)
{
    return static_cast<glm::ivec2>(position / static_cast<glm::vec2>(size()));
}

bool HalfTiles::withinTile(const glm::vec2& position, const glm::ivec2& tile)
{
    return toTile(position) == tile;
}

const glm::ivec2& HalfTiles::size()
{
    static glm::ivec2 tileSize =  Tiles::size() / 2;

    return tileSize;
}
