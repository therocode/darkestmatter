#pragma once
#include "util/grid.hpp"
#include "world/wallore.hpp"
#include "landscapetypes.hpp"

class LandscapeCallbackHandler;

class Landscape
{
    public:
        Landscape() = default;
        Landscape(const glm::ivec2& size, LandscapeCallbackHandler* gameWorld = nullptr);
        //ground
        void setGround(const glm::ivec2& position, GroundType type);
        const Grid<GroundType>& ground() const;
        //floors
        void setFloor(const glm::ivec2& position, FloorType type);
        const Grid<FloorType>& floors() const;
        //walls
        void setWall(const glm::ivec2& position, WallType type);
        const Grid<WallType>& walls() const;
        //wall ore
        void setWallOre(const glm::ivec2& position, const WallOre& ore);
        const Grid<WallOre>& wallOres() const;
        //misc
        bool inBounds(const glm::ivec2& position) const;
        void setCallbackHandler(LandscapeCallbackHandler& gameWorld);
    private:
        Grid<GroundType> mGround;
        Grid<FloorType> mFloors;
        Grid<WallType> mWalls;
        Grid<WallOre> mWallOre;
        LandscapeCallbackHandler* mCallbackHandler;
};
