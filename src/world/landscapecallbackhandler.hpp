#pragma once
#include "landscapetypes.hpp"
#include <glm/glm.hpp>

class LandscapeCallbackHandler
{
    public:
        virtual void wallSet(const glm::ivec2& position, WallType type) = 0;
        virtual void groundSet(const glm::ivec2& position, GroundType type) = 0;
        virtual void floorSet(const glm::ivec2& position, FloorType type) = 0;
        virtual void wallOreSet(const glm::ivec2& position, WallOre ore) = 0;
};
