#pragma once
#include <deque>
#include <glm/glm.hpp>
#include <fea/rendering/glmhash.hpp>
#include <thero/polymorphicwrapper.hpp>
#include "items/itemtype.hpp"
#include "items/item.hpp"
#include "items/iteminvoker.hpp"
#include "items/relationshipdatabase.hpp"
#include "itemtypedata.hpp"
#include <util/numberpool.hpp>

class GameWorld;

class ItemManager
{
    struct ItemStack
    {
        const ItemType& type;
        std::deque<Item> items;
    };

    public:
        struct ItemStackView
        {
            std::reference_wrapper<const ItemStack> stack;
            int32_t stackId;
            int32_t typeId;
            glm::ivec2 position;
        };

        using ItemStackList = std::deque<ItemStackView>;

        ItemManager(const RelationshipDatabase& propertyRelations, std::unordered_map<int32_t, ItemTypeData> itemTypes, GameWorld& world);
        Item createItem(int32_t itemType);
        th::Optional<Item> putItem(Item item, const glm::ivec2& position);
        th::Optional<ItemStackView> stackAt(const glm::ivec2& position) const;
        th::Optional<Item> takeItem(const glm::ivec2& position, int32_t stackIndex = 0);
        ItemStackList findItems(th::Optional<int32_t> itemType = {});
        ItemStackList findItems(ItemProperty property);
        ItemStackList findItems(const std::unordered_set<glm::ivec2>& tiles, th::Optional<int32_t> itemType = {});
        ItemStackList findItems(const std::unordered_set<glm::ivec2>& tiles, ItemProperty property);
        const ItemType& itemType(int32_t itemTypeID);
        th::Optional<ItemStackView> stack(int32_t stackId) const;
        th::Optional<AnyMap<std::string>> invokeItem(int32_t stackId, int32_t stackIndex, ItemAction action, ItemInvoker& invoker, AnyMap<std::string> parameters);
        bool typeSatisfiesProperty(const ItemType& type, ItemProperty property) const;
        th::Optional<ItemStackView> renewStackView(const ItemStackView& stackView) const;
    private:
        glm::ivec2 getTopLeftOfTile(glm::ivec2 coordinate) const;
        glm::ivec2 stackPosition(int32_t stackId) const;
        const RelationshipDatabase& mPropertyRelations;
        GameWorld& mGameWorld;

        //item type stuff
        std::unordered_map<std::string, th::PolymorphicWrapper<ItemInvokerHandler>> mInvokeHandlers;
        std::unordered_map<int32_t, ItemType> mItemTypes;

        //items on the ground in the world
        NumberPool<int32_t> mItemStackIdPool;
        std::unordered_map<int32_t, ItemStack> mItemStacks;
        std::unordered_map<glm::ivec2, int32_t> mItemStackIndex; //big items cover 4 tiles
};
