#pragma once
#include <unordered_map>
#include <fea/render2d.hpp>

using TextureMap = std::unordered_map<int32_t, fea::Texture>;
