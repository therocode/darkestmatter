#pragma once
#include "texturemap.hpp"

class DrawableProvider
{
    public:
        DrawableProvider(const TextureMap& textures);
        virtual ~DrawableProvider() = default;
        virtual void render(fea::Renderer2D& renderer) const = 0;
        virtual void tick(int32_t frames) = 0;
        virtual void textureAdded(int32_t id, fea::Texture& texture, const std::string& textureName) {};
        void setViewSize(const glm::vec2& viewSize);
    protected:
        const TextureMap& mTextures;
        glm::vec2 mViewSize;
};
