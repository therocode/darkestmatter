#pragma once
#include "drawableprovider.hpp"
#include "constructionrendertype.hpp"
#include <util/numberpool.hpp>

class ConstructionProvider : public DrawableProvider
{
    struct Construction
    {
        int64_t type;
        fea::SubrectQuad quad;
    };

    public:
        ConstructionProvider(const TextureMap& textures, const glm::ivec2& tileWidth);
        void registerConstructionRenderType(int64_t typeId, ConstructionRenderType type);
        int32_t createConstruction(int64_t typeId, const glm::ivec2& tilePosition);
        void destroyConstruction(int32_t objectId);
        void render(fea::Renderer2D& renderer) const override;
        void tick(int32_t frames) override;
    private:
        NumberPool<int32_t> mIdPool;
        std::unordered_map<int64_t, ConstructionRenderType> mObjectTypes;
        std::unordered_map<int32_t, Construction> mObjects;
        glm::ivec2 mTileWidth;
};
