#pragma once

namespace gim
{
    class Element;
}

namespace fea
{
    class Color;
}

struct OverlayCreatedMessage
{
    int32_t overlayId;
    const std::unordered_map<glm::ivec2, fea::Color>& overlayTiles;
};

struct OverlayDestroyedMessage
{
    int32_t overlayId;
};

struct UIRenderRequestedMessage
{
    const gim::Element& uiRoot;
};
