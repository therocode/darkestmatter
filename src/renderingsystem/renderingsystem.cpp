#include "renderingsystem.hpp"
#include "world/tiles.hpp"

RenderingSystem::RenderingSystem(const fea::Viewport& viewport, fea::MessageBus& bus, const glm::ivec2& tileSize):
    mRenderer(viewport),
    mCamera(viewport.getCamera()),
    mBus(bus),
    mDoTick(true),
    mAnimatedObjectProvider(mTextures),
    mConstructionProvider(mTextures, tileSize),
    mWorldHelperProvider(mTextures, tileSize),
    mItemProvider(mTextures),
    mLandscapeProvider(mTextures, tileSize, tileSize / 2),
    mUIProvider(mTextures)
{
    subscribe(mBus, *this);

    mLandscapeProvider.setCullEnabled(true);

    mProviderPointers.emplace_back(&mLandscapeProvider);
    mProviderPointers.emplace_back(&mConstructionProvider);
    mProviderPointers.emplace_back(&mItemProvider);
    mProviderPointers.emplace_back(&mAnimatedObjectProvider);
    mProviderPointers.emplace_back(&mWorldHelperProvider);
    mProviderPointers.emplace_back(&mUIProvider);

    setViewport(viewport);
}

void RenderingSystem::setViewport(const fea::Viewport& viewport)
{
    mRenderer.setViewport(viewport);
    mCamera = ViewCamera(viewport.getCamera());

    for(auto provider : mProviderPointers)
    {
        provider->setViewSize((glm::vec2)viewport.getSize());
    }
}

void RenderingSystem::render()
{
    mCamera.update();
    mRenderer.getViewport().setCamera(mCamera);

    updateCulling();

    mRenderer.clear();

    for(auto provider : mProviderPointers)
    {
        provider->render(mRenderer);
        if(mDoTick)
            provider->tick(1);
    }
}

const fea::Renderer2D& RenderingSystem::renderer() const
{
    return mRenderer;
}

void RenderingSystem::addTexture(int32_t textureId, fea::Texture texture, const std::string& textureName)
{
    fea::Texture& addedTexture = mTextures.emplace(textureId, std::move(texture)).first->second;

    for(auto provider : mProviderPointers)
    {
        provider->textureAdded(textureId, addedTexture, textureName);
    }
}

void RenderingSystem::addFont(int32_t fontId, gim::Font font, const std::string& fontName)
{
    LOG_I("adding font " << font.name() << " to renderer");
    mUIProvider.addFont(fontId, std::move(font), fontName);
}

void RenderingSystem::disableTick(bool disable)
{
    mDoTick = !disable;
}

void RenderingSystem::handleMessage(const ResizeMessage& message)
{
    setViewport(fea::Viewport({message.size.x, message.size.y}, {0, 0}, fea::Camera({static_cast<float>(message.size.x) / 2.0f, static_cast<float>(message.size.y) / 2.0f})));
}

void RenderingSystem::handleMessage(const ViewPanDirectionMessage& message)
{
    mCamera.pan(message.direction);
}

void RenderingSystem::handleMessage(const ViewZoomedMessage& message)
{
    mCamera.zoom(message.delta);
}

void RenderingSystem::addWallType(int32_t wallType, int32_t textureId, const glm::ivec2& textureIndex)
{
    mLandscapeProvider.addWallType(wallType, textureId, textureIndex);
}

void RenderingSystem::addWallOreType(int32_t wallOreType, WallOreTileMap::OreAmountThreshold thresholds, int32_t textureId, const glm::ivec2& textureIndex)
{
    mLandscapeProvider.addWallOreType(wallOreType, thresholds, textureId, textureIndex);
}

void RenderingSystem::addGroundType(int32_t groundType, int32_t textureId, const glm::ivec2& textureIndex)
{
    mLandscapeProvider.addGroundType(groundType, textureId, textureIndex);
}

void RenderingSystem::addFloorType(int32_t floorType, int32_t textureId, const glm::ivec2& textureIndex)
{
    mLandscapeProvider.addFloorType(floorType, textureId, textureIndex);
}

void RenderingSystem::handleMessage(const LandscapeLoadedMessage& message)
{
    mLandscapeProvider.setLandscape(message.landscape);
}

void RenderingSystem::handleMessage(const WallTileSetMessage& message)
{
    mLandscapeProvider.setWall(message.position, message.type);
}

void RenderingSystem::handleMessage(const GroundTileSetMessage& message)
{
    mLandscapeProvider.setGround(message.position, message.type);
}

void RenderingSystem::handleMessage(const FloorTileSetMessage& message)
{
    mLandscapeProvider.setFloor(message.position, message.type);
}

void RenderingSystem::addItemType(int32_t typeId, ItemTypeGfx type)
{
    mItemProvider.registerItemType(typeId, std::move(type));
}

void RenderingSystem::handleMessage(const ItemStackCreatedMessage& message)
{
    FEA_ASSERT(mItemIdMap.count(message.itemStackId) == 0, "Graphics for item stack " << message.itemStackId << " already created!");
    int32_t returnedId = mItemProvider.createItem(message.itemType, message.position, message.stackAmount);
    mItemIdMap.emplace(message.itemStackId, returnedId);
}

void RenderingSystem::handleMessage(const ItemStackUpdatedMessage& message)
{
    FEA_ASSERT(mItemIdMap.count(message.itemStackId) != 0, "Cannot set stackAmount on item stack id " << message.itemStackId << " since it doesn't exist");
    int32_t id = mItemIdMap.at(message.itemStackId);
    mItemProvider.setItemStackAmount(id, message.stackAmount);
}

void RenderingSystem::handleMessage(const ItemStackDepletedMessage& message)
{
    FEA_ASSERT(mItemIdMap.count(message.itemStackId) != 0, "Cannot destroy item stack id " << message.itemStackId << " since it doesn't exist");
    int32_t id = mItemIdMap.at(message.itemStackId);
    mItemProvider.destroyItem(id);
    mItemIdMap.erase(id);
}

void RenderingSystem::addAnimatedSpriteType(int32_t spriteId, AnimatedObjectType sprite)
{
    mAnimatedObjectProvider.registerAnimatedObjectType(spriteId, std::move(sprite));
}

void RenderingSystem::handleMessage(const AnimatedSpriteCreatedMessage& message)
{
    FEA_ASSERT(mAnimatedSpriteIdMap.count(message.entityId) == 0, "Graphics for sprite " << message.entityId << " already created!");
    int32_t returnedId = mAnimatedObjectProvider.createAnimatedObject(message.entityGraphicsType, message.position);
    mAnimatedSpriteIdMap.emplace(message.entityId, returnedId);
}

void RenderingSystem::handleMessage(const AnimatedSpriteMovedMessage& message)
{
    FEA_ASSERT(mAnimatedSpriteIdMap.count(message.entityId) != 0, "Cannot move animated sprite id " << message.entityId << " since it doesn't exist");
    int32_t id = mAnimatedSpriteIdMap.at(message.entityId);
    mAnimatedObjectProvider.setAnimatedObjectPosition(id, message.position);
}

void RenderingSystem::handleMessage(const AnimatedSpriteDestroyedMessage& message)
{
    FEA_ASSERT(mAnimatedSpriteIdMap.count(message.entityId) != 0, "Cannot destroy animated sprite id " << message.entityId << " since it doesn't exist");
    int32_t id = mAnimatedSpriteIdMap.at(message.entityId);
    mAnimatedObjectProvider.destroyAnimatedObject(id);
    mAnimatedSpriteIdMap.erase(message.entityId);
}

void RenderingSystem::handleMessage(const OverlayCreatedMessage& message)
{
    FEA_ASSERT(mOverlayIdMap.count(message.overlayId) == 0, "Overlay with id " << message.overlayId << " already created!");
    int32_t returnedId = mWorldHelperProvider.createTileOverlay(message.overlayTiles);
    mOverlayIdMap.emplace(message.overlayId, returnedId);
}

void RenderingSystem::handleMessage(const OverlayDestroyedMessage& message)
{
    FEA_ASSERT(mOverlayIdMap.count(message.overlayId) != 0, "Cannot destroy overlay id " << message.overlayId << " since it doesn't exist");
    int32_t id = mOverlayIdMap.at(message.overlayId);
    mWorldHelperProvider.destroyTileOverlay(id);
    mOverlayIdMap.erase(message.overlayId);
}

void RenderingSystem::addConstructionSpriteType(int32_t spriteType, ConstructionRenderType constructionTypeData)
{
    mConstructionProvider.registerConstructionRenderType(spriteType, constructionTypeData);
}

void RenderingSystem::addConstructionType(ConstructionType constructionType, int32_t spriteId)
{
    FEA_ASSERT(mConstructionSpriteTypes.count(constructionType) == 0, "construction type already registered");
    mConstructionSpriteTypes.emplace(constructionType, spriteId);
}

void RenderingSystem::handleMessage(const ConstructionCreatedMessage& message)
{
    FEA_ASSERT(mConstructionIdMap.count(message.id) == 0, "Graphics for construction " << message.id << " already created!");
    FEA_ASSERT(mConstructionSpriteTypes.count(message.type) != 0, "No construction graphics registered for '" << message.type.category << "' id '" << message.type.type << "\n");
    int32_t returnedId = mConstructionProvider.createConstruction(mConstructionSpriteTypes.at(message.type), message.position);
    mConstructionIdMap.emplace(message.id, returnedId);
}

void RenderingSystem::handleMessage(const ConstructionFinishedMessage& message)
{
    FEA_ASSERT(mConstructionIdMap.count(message.id) != 0, "Cannot destroy construction id " << message.id << " since it doesn't exist");
    int32_t id = mConstructionIdMap.at(message.id);
    mConstructionProvider.destroyConstruction(id);
    mConstructionIdMap.erase(message.id);
}

void RenderingSystem::handleMessage(const UIRenderRequestedMessage& message)
{
    mUIProvider.queueElement(message.uiRoot);
}

void RenderingSystem::updateCulling()
{
    auto viewStart = mRenderer.getViewport().getPosition() - glm::ivec2(0, 0);
    auto viewSize = static_cast<glm::ivec2>(mRenderer.getViewport().getSize()) + glm::ivec2(0, 0);

    glm::vec2 viewStartWorld = mRenderer.getViewport().untransformPoint((glm::vec2)viewStart);
    glm::vec2 viewEndWorld = mRenderer.getViewport().untransformPoint((glm::vec2)(viewStart + viewSize));


    mLandscapeProvider.setCullRegion(viewStartWorld, viewEndWorld);
}
