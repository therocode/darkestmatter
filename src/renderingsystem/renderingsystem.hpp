#pragma once
#include "animatedobjectprovider.hpp"
#include "constructionprovider.hpp"
#include "landscapeprovider.hpp"
#include "itemprovider.hpp"
#include "worldhelperprovider.hpp"
#include "uiprovider.hpp"
#include "texturemap.hpp"
#include "drawableprovider.hpp"
#include "renderingsystem/renderingmessages.hpp"
#include "messages.hpp"
#include "world/worldmessages.hpp"
#include "itemtypegfx.hpp"
#include "animatedobjecttype.hpp"
#include "constructionrendertype.hpp"
#include "viewcamera.hpp"

class RenderingSystem: public fea::MessageReceiver<
                       ResizeMessage,
                       ViewPanDirectionMessage,
                       ViewZoomedMessage,
                       LandscapeLoadedMessage,
                       WallTileSetMessage,
                       GroundTileSetMessage,
                       FloorTileSetMessage,
                       ItemStackCreatedMessage,
                       ItemStackUpdatedMessage,
                       ItemStackDepletedMessage,
                       AnimatedSpriteCreatedMessage,
                       AnimatedSpriteMovedMessage,
                       AnimatedSpriteDestroyedMessage,
                       OverlayCreatedMessage,
                       OverlayDestroyedMessage,
                       ConstructionCreatedMessage,
                       ConstructionFinishedMessage,
                       UIRenderRequestedMessage>
{
    public:
        RenderingSystem(const fea::Viewport& viewport, fea::MessageBus& bus, const glm::ivec2& tileSize);
        void setViewport(const fea::Viewport& viewport);
        void render();
        const fea::Renderer2D& renderer() const;
        void addTexture(int32_t textureId, fea::Texture texture, const std::string& textureName);
        void addFont(int32_t fontId, gim::Font font, const std::string& fontName);
        void disableTick(bool disable);
        void handleMessage(const ResizeMessage& message) override;
        void handleMessage(const ViewPanDirectionMessage& message) override;
        void handleMessage(const ViewZoomedMessage& message) override;
        //tiles
        void addWallType(int32_t wallType, int32_t textureId, const glm::ivec2& textureIndex);
        void addWallOreType(int32_t wallOreType, WallOreTileMap::OreAmountThreshold thresholds, int32_t textureId, const glm::ivec2& textureIndex);
        void addGroundType(int32_t groundType, int32_t textureId, const glm::ivec2& textureIndex);
        void addFloorType(int32_t floorType, int32_t textureId, const glm::ivec2& textureIndex);
        void handleMessage(const LandscapeLoadedMessage& message) override;
        void handleMessage(const WallTileSetMessage& message) override;
        void handleMessage(const GroundTileSetMessage& message) override;
        void handleMessage(const FloorTileSetMessage& message) override;
        //items
        void addItemType(int32_t typeId, ItemTypeGfx type);
        void handleMessage(const ItemStackCreatedMessage& message) override;
        void handleMessage(const ItemStackUpdatedMessage& message) override;
        void handleMessage(const ItemStackDepletedMessage& message) override;
        //animated sprites
        void addAnimatedSpriteType(int32_t spriteId, AnimatedObjectType sprite);
        void handleMessage(const AnimatedSpriteCreatedMessage& message) override;
        void handleMessage(const AnimatedSpriteMovedMessage& message) override;
        void handleMessage(const AnimatedSpriteDestroyedMessage& message) override;
        //overlay
        void handleMessage(const OverlayCreatedMessage& message) override;
        void handleMessage(const OverlayDestroyedMessage& message) override;
        //constructions
        void addConstructionSpriteType(int32_t spriteId, ConstructionRenderType constructionTypeData);
        void addConstructionType(ConstructionType constructionType, int32_t spriteId);
        void handleMessage(const ConstructionCreatedMessage& message) override;
        void handleMessage(const ConstructionFinishedMessage& message) override;
        //ui
        void handleMessage(const UIRenderRequestedMessage& message) override;
    private:
        void updateCulling();
        fea::Renderer2D mRenderer;
        ViewCamera mCamera;
        TextureMap mTextures;
        fea::MessageBus& mBus;
        bool mDoTick;

        AnimatedObjectProvider mAnimatedObjectProvider;
        ConstructionProvider mConstructionProvider;
        WorldHelperProvider mWorldHelperProvider;
        ItemProvider mItemProvider;
        LandscapeProvider mLandscapeProvider;
        UIProvider mUIProvider;
        std::vector<DrawableProvider*> mProviderPointers;

        //items
        std::unordered_map<int32_t, int32_t> mItemIdMap;
        //animated sprites
        std::unordered_map<int32_t, int32_t> mAnimatedSpriteIdMap;
        //overlays
        std::unordered_map<int32_t, int32_t> mOverlayIdMap;
        //constructions
        std::unordered_map<int32_t, int32_t> mConstructionIdMap;
        std::unordered_map<ConstructionType, int32_t> mConstructionSpriteTypes;
};
