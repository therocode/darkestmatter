#pragma once
#include "drawableprovider.hpp"
#include "drawables/guielement.hpp"

namespace gim
{
    class Element;
}

class UIProvider : public DrawableProvider
{
    public:
        UIProvider(const TextureMap& textures);
        void render(fea::Renderer2D& renderer) const override;
        void tick(int32_t frames) override;
        void queueElement(const gim::Element& element);
        void textureAdded(int32_t id, fea::Texture& texture, const std::string& textureName) override;
        void addFont(int32_t fontId, gim::Font font, const std::string& fontName);
    private:
        const gim::Element* mElementToRender;
        mutable GuiElement mGuiDrawable;
        std::vector<std::unique_ptr<gim::Font>> mFonts;
};
