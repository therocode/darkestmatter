#include "animatedobjectprovider.hpp"
#include <fea/assert.hpp>

AnimatedObjectProvider::AnimatedObjectProvider(const TextureMap& textures):
    DrawableProvider(textures)
{
}

void AnimatedObjectProvider::registerAnimatedObjectType(int32_t typeId, AnimatedObjectType type)
{
    FEA_ASSERT(mTextures.count(type.texture) != 0, "invalid texture given");
    FEA_ASSERT(mObjectTypes.count(typeId) == 0, "typeId already used");
    FEA_ASSERT(type.animations.count(type.defaultAnimation) != 0, "Default animation does not exist among animations given");

    mObjectTypes.emplace(typeId, std::move(type));
}

int32_t AnimatedObjectProvider::createAnimatedObject(int32_t typeId, const glm::vec2& position)
{
    FEA_ASSERT(mObjectTypes.count(typeId) != 0, "typeId not registered");
    int32_t newId = mIdPool.next();

    const auto& objectType = mObjectTypes.at(typeId);

    fea::AnimatedQuad newObject(objectType.size);
    newObject.setPosition(position);
    newObject.setOrigin(newObject.getSize() / 2.0f - static_cast<glm::vec2>(objectType.offset));
    newObject.setTexture(mTextures.at(objectType.texture));
    newObject.setAnimation(objectType.animations.at(objectType.defaultAnimation));

    mObjects.emplace(newId, AnimatedObject{typeId, newObject});

    return newId;
}

void AnimatedObjectProvider::destroyAnimatedObject(int32_t objectId)
{
    FEA_ASSERT(mObjects.count(objectId) != 0, "Destroying nonexisting object");

    mObjects.erase(objectId);
}

void AnimatedObjectProvider::render(fea::Renderer2D& renderer) const
{
    for(auto& animatedObject : mObjects)
    {
        renderer.render(animatedObject.second.quad);
    }
}

void AnimatedObjectProvider::tick(int32_t frames)
{
    for(int32_t i = 0; i < frames; i++)
    {
        for(auto& animatedQuad : mObjects)
            animatedQuad.second.quad.tick();
    }
}

void AnimatedObjectProvider::setAnimatedObjectPosition(int32_t objectId, const glm::vec2& position)
{
    FEA_ASSERT(mObjects.count(objectId) != 0, "Setting position of nonexisting object");

    mObjects.at(objectId).quad.setPosition(glm::floor(position));
}

void AnimatedObjectProvider::setAnimatedObjectAnimation(int32_t objectId, int32_t animation)
{
    FEA_ASSERT(mObjects.count(objectId) != 0, "Setting animation of nonexisting object");

    auto& object = mObjects.at(objectId);
    auto& type = mObjectTypes.at(object.type);

    FEA_ASSERT(type.animations.count(animation) != 0, "Setting nonexisting animation on object");

    object.quad.setAnimation(type.animations.at(animation));
}
