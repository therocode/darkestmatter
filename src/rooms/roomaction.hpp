#pragma once

smart_enum_class(RoomAction, We_got_no_action)

namespace std
{
    template<> struct hash<RoomAction>
    {
        size_t operator()(const RoomAction& x) const
        {
            return static_cast<size_t>(x);
        }
    };
}
