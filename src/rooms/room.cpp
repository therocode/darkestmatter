#include "rooms/room.hpp"
#include "world/tiles.hpp"
#include <fea/assert.hpp>

Room::Room(const RoomType& type, AnyMap<std::string> attributes, const std::unordered_set<glm::ivec2>& tiles):
    mType(type),
    mDestroyed(false),
    mAttributes(std::move(attributes)),
    mTiles(tiles)
{
}

th::Optional<AnyMap<std::string>> Room::invoke(RoomAction action, RoomInvoker& invoker, AnyMap<std::string> parameters)
{
    FEA_ASSERT(!mDestroyed, "Invoking destroyed room is a bug");

    if(!mDestroyed)
    {
        auto invokerIter = mType.get().mInvokeHandlers.find(action);
        if(invokerIter != mType.get().mInvokeHandlers.end())
        {
            auto ret = invokerIter->second.get().invoke(action, *this, invoker, std::move(parameters));
            return ret;
        }
        else
            return th::Optional<AnyMap<std::string>>{{}};
    }
    else
    {
        return {};
    }
}

void Room::destroy(const RoomInvokerHandler&)
{
    mDestroyed = true;
}

const AnyMap<std::string>& Room::attributes() const
{
    return mAttributes;
}

AnyMap<std::string>& Room::attributes()
{
    return mAttributes;
}

const std::unordered_set<glm::ivec2>& Room::tiles() const
{
    return mTiles;
}

std::unordered_set<glm::ivec2> Room::halfTiles() const
{
    return Tiles::tilesToHalfTiles(tiles());
}

const RoomType& Room::type() const
{
    return mType;
}

bool Room::destroyed() const
{
    return mDestroyed;
}
