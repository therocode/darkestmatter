#pragma once

#include "items/invokehandler.hpp"
#include "roomaction.hpp"

class RoomInvoker
{
};

class Room;

using RoomInvokerHandler = InvokeHandler<Room, RoomAction, RoomInvoker>;
