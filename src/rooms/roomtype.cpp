#include "roomtype.hpp"

bool operator==(const RoomType& a, const RoomType& b)
{
    return a.typeId == b.typeId;
}
