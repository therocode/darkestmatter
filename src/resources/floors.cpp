#include "floors.hpp"

const std::unordered_map<std::string, FloorResource> rFloors =
{
    {
        "steel_floor",
        {
            "Steel floor",
            "This is a very thick floor out of steel!",
            "floors",
            {1, 1},
            100,
            "floor_construction",
            true,
            {
                {
                    "iron_bar",
                    1,
                },
            },
        },
    },
};
