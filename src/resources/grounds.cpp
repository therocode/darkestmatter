#include "grounds.hpp"

const std::unordered_map<std::string, GroundResource> rGrounds = 
{
    {
        "dry_rock",
        {
            "Dry rock ground",
            "Dusty and dry",
            "floors",
            {0, 0},
        },
    },
};
