#pragma once
#include <unordered_map>
#include <unordered_set>
#include "itemgfxresource.hpp"
#include "items/itemaction.hpp"
#include "items/itemsize.hpp"

smart_enum(ItemDataType, INT, STRING, FLOAT)

using ItemDefaultData = std::pair<ItemDataType, std::string>;

struct ItemResource
{
    int64_t weight;
    int32_t maxStack;
    ItemSize size;
    bool big;
    std::unordered_set<std::string> properties;
    std::unordered_map<std::string, ItemDefaultData> defaultData;
    std::unordered_map<ItemAction, std::string> invokeHandlers;
    ItemGfxResource gfx;
};

extern std::unordered_map<std::string, ItemResource> rItems;
