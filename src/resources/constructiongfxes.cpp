#include "constructiongfxes.hpp"

std::unordered_map<std::string, ConstructionGfxResource> rConstructionGfxes = 
{
    {
        "wall_construction",
        {
            "constructions",          //texture
            {0, 0},                //start
            {32, 32},              //size
        },
    }, {
        "floor_construction",
        {
            "constructions",          //texture
            {32, 0},                //start
            {32, 32},              //size
        },
    },
};
