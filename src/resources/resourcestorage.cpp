#include "resourcestorage.hpp"

int32_t ResourceStorage::addFont(std::string name, std::string fontPath)
{
    int32_t id = static_cast<int32_t>(mFonts.size());
    mFonts.emplace(id, std::move(fontPath));
    mFontIds.emplace(name, id);
    return id;
}

const ResourceStorage::IdMap& ResourceStorage::fontIds() const
{
    return mFontIds;
}

const ResourceStorage::FontMap& ResourceStorage::fonts() const
{
    return mFonts;
}

int32_t ResourceStorage::addTexture(std::string name, std::string texturePath)
{
    int32_t id = static_cast<int32_t>(mTextures.size());
    mTextures.emplace(id, std::move(texturePath));
    mTextureIds.emplace(name, id);
    return id;
}

const ResourceStorage::IdMap& ResourceStorage::textureIds() const
{
    return mTextureIds;
}

const ResourceStorage::TextureMap& ResourceStorage::textures() const
{
    return mTextures;
}

int32_t ResourceStorage::addWall(std::string name, WallResource wall)
{
    int32_t id = static_cast<int32_t>(mWalls.size() + 1); //0 reserved for air
    mWalls.emplace(id, std::move(wall));
    mWallIds.emplace(name, id);
    return id;
}

const ResourceStorage::IdMap& ResourceStorage::wallIds() const
{
    return mWallIds;
}

const ResourceStorage::WallMap& ResourceStorage::walls() const
{
    return mWalls;
}

int32_t ResourceStorage::addWallOre(std::string name, WallOreResource wallOre)
{
    int32_t id = static_cast<int32_t>(mWallOre.size() + 1); //0 reserved for air
    mWallOre.emplace(id, std::move(wallOre));
    mWallOreIds.emplace(name, id);
    return id;
}

const ResourceStorage::IdMap& ResourceStorage::wallOreIds() const
{
    return mWallOreIds;
}

const ResourceStorage::WallOreMap& ResourceStorage::wallOre() const
{
    return mWallOre;
}

int32_t ResourceStorage::addFloor(std::string name, FloorResource floor)
{
    int32_t id = static_cast<int32_t>(mFloors.size() + 1); //0 reserved for nothing
    mFloors.emplace(id, std::move(floor));
    mFloorIds.emplace(name, id);
    return id;
}

const ResourceStorage::IdMap& ResourceStorage::floorIds() const
{
    return mFloorIds;
}

const ResourceStorage::FloorMap& ResourceStorage::floors() const
{
    return mFloors;
}

int32_t ResourceStorage::addGround(std::string name, GroundResource ground)
{
    int32_t id = static_cast<int32_t>(mGrounds.size() + 1); //0 reserved for nothing
    mGrounds.emplace(id, std::move(ground));
    mGroundIds.emplace(name, id);
    return id;
}

const ResourceStorage::IdMap& ResourceStorage::groundIds() const
{
    return mGroundIds;
}

const ResourceStorage::GroundMap& ResourceStorage::grounds() const
{
    return mGrounds;
}

int32_t ResourceStorage::addItem(std::string name, ItemResource item)
{
    int32_t id = static_cast<int32_t>(mItems.size());
    mItems.emplace(id, std::move(item));
    mItemIds.emplace(name, id);
    return id;
}

const ResourceStorage::ResourceStorage::IdMap& ResourceStorage::itemIds() const
{
    return mItemIds;
}

const ResourceStorage::ItemMap& ResourceStorage::items() const
{
    return mItems;
}

int32_t ResourceStorage::addAnimatedSprite(std::string name, EntityGfxResource animatedSprite)
{
    int32_t id = static_cast<int32_t>(mAnimatedSprites.size());
    mAnimatedSprites.emplace(id, std::move(animatedSprite));
    mAnimatedSpriteIds.emplace(name, id);
    return id;
}

const ResourceStorage::IdMap& ResourceStorage::animatedSpriteIds() const
{
    return mAnimatedSpriteIds;
}

const ResourceStorage::AnimatedSpriteMap& ResourceStorage::animatedSprites() const
{
    return mAnimatedSprites;
}

int32_t ResourceStorage::addConstructionSprite(std::string name, ConstructionGfxResource constructionSprite)
{
    int32_t id = static_cast<int32_t>(mConstructionSprites.size());
    mConstructionSprites.emplace(id, std::move(constructionSprite));
    mConstructionSpriteIds.emplace(name, id);
    return id;
}

const ResourceStorage::IdMap& ResourceStorage::constructionSpriteIds() const
{
    return mConstructionSpriteIds;
}

const ResourceStorage::ConstructionSpriteMap& ResourceStorage::ConstructionSprites() const
{
    return mConstructionSprites;
}
