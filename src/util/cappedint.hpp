#pragma once
#include <cstdint>

class CappedInt
{
    public:
        CappedInt(int32_t min, int32_t max, int32_t number = 0);
        operator int32_t() const;
        CappedInt& operator =(int32_t number);
        CappedInt& operator ++();
        CappedInt& operator --();
        CappedInt& operator +=(int32_t number);
        CappedInt& operator -=(int32_t number);
        CappedInt& operator *=(int32_t number);
        CappedInt& operator /=(int32_t number);
    private:
        int32_t mNumber;
        int32_t mMin;
        int32_t mMax;
};
