#pragma once

template<typename Iterator, typename RandomGenerator = std::mt19937>
Iterator selectRandom(Iterator begin, Iterator end)
{
    std::random_device randomDevice;
    RandomGenerator generator(0);//randomDevice());

    std::uniform_int_distribution<int64_t> advanceAmount(0, std::distance(begin, end) - 1);

    std::advance(begin, advanceAmount(generator));
    return begin;
}
