#pragma once

template<typename Type>
class Pimpl
{
    public:
        Pimpl():
            mValue(std::make_unique<Type>())
        {
        }

        template<typename ...Args>
        Pimpl(Args&& ...arguments)
        {
            mValue = std::make_unique<Type>(arguments...);
        }

        Pimpl(Type&& value)
        {
            mValue = std::make_unique<Type>(std::move(value));
        }

        Pimpl(const Pimpl& other):
            mValue(std::make_unique<Type>(*other.mValue))
        {

        }

        Pimpl(Pimpl& other):
            Pimpl(static_cast<const Pimpl&>(other))
        {
        }

        Pimpl(Pimpl&& other):
            mValue(std::move(other.mValue))
        {
        }

        Pimpl& operator=(const Pimpl& other)
        {
            mValue = std::make_unique<Type>(*other.mValue);
            return *this;
        }

        Pimpl& operator=(Pimpl& other)
        {
            return *this = static_cast<const Pimpl&>(other);
        }

        Pimpl& operator=(Pimpl&& other)
        {
            mValue = std::move(other.mValue);
            return *this;
        }

        const Type& operator*() const
        {
            return *mValue;
        }

        Type& operator*()
        {
            return *mValue;
        }

        const Type* operator->() const
        {
            return mValue.get();
        }

        Type* operator->()
        {
            return mValue.get();
        }

    private:
        std::unique_ptr<Type> mValue; };
