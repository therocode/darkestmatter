#pragma once
#include <unordered_set>
#include <glm/glm.hpp>
#include <deque>

struct WallResource;

class GameUI
{
    public:
        enum class HighlightMode
        {
            IDLE_WALL_ORDER, BUSY_WALL_ORDER,
            IDLE_FLOOR_ORDER, BUSY_FLOOR_ORDER,
            IDLE_MINE_ORDER, BUSY_MINE_ORDER
        };

        virtual void highlightTiles(std::unordered_set<glm::ivec2> tiles, HighlightMode mode) = 0;
        virtual void dehighlightTiles(std::unordered_set<glm::ivec2> tiles) = 0;
        virtual void update() = 0;
        virtual void setAvailableWalls(std::deque<std::reference_wrapper<const WallResource>> currentWallKnowledge) = 0;
        virtual void resize(const glm::ivec2 newSize) = 0;
};
