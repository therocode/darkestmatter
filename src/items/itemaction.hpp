#pragma once
#include <thero/smartenum.hpp>

smart_enum_class(ItemAction, EAT, TRIGGER, PUT, INSPECT, TAKE_OUT)

namespace std
{
    template<> struct hash<ItemAction>
    {
        size_t operator()(const ItemAction& x) const
        {
            return static_cast<size_t>(x);
        }
    };
}
