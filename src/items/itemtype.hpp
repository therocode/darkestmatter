#pragma once
#include <unordered_set>
#include "itemproperties.hpp"
#include "itemaction.hpp"
#include "items/iteminvoker.hpp"
#include "itemsize.hpp"

using ItemTypeId = int32_t;
//class InvokeHandler;

struct ItemType
{
    ItemTypeId typeId;
    int64_t weight;
    ItemSize size;
    int32_t maxStack;
    bool big;
    std::unordered_set<ItemProperty> properties;
    std::unordered_map<ItemAction, std::reference_wrapper<ItemInvokerHandler>> mInvokeHandlers;
    AnyMap<std::string> defaultValues;
};

bool operator==(const ItemType& a, const ItemType& b);
