#pragma once

using ItemSize = int64_t;

namespace Sizes
{
    constexpr ItemSize DIE = 10;
    constexpr ItemSize EGG = DIE * 6;
    constexpr ItemSize CUP = EGG * 3;
    constexpr ItemSize SOCCER_BALL = CUP * 8;
    constexpr ItemSize MICROWAVE = SOCCER_BALL * 3;
    constexpr ItemSize TABLE_CHAIR = MICROWAVE * 8;
    constexpr ItemSize BATH_TUB = TABLE_CHAIR * 4;
    constexpr ItemSize DOUBLE_BED = BATH_TUB * 3;
    constexpr ItemSize CAR = DOUBLE_BED * 2;
    constexpr ItemSize TRUCK = CAR * 4;
}
