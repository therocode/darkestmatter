#pragma once
#include <util/anymap.hpp>

template <typename InvokedType, typename ActionType, typename InvokerType>
class InvokeHandler
{
    public:
        virtual ~InvokeHandler() = default;
        virtual AnyMap<std::string> invoke(ActionType action, InvokedType& invoked, InvokerType& invoker, AnyMap<std::string> parameters) = 0;
};
