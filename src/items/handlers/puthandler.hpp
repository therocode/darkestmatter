#pragma once
#include "../iteminvoker.hpp"

class PutHandler : public ItemInvokerHandler
{
    public:
        AnyMap<std::string> invoke(ItemAction action, Item& item, ItemInvoker& invoker, AnyMap<std::string> parameters) override;
};
