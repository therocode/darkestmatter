#include "inspecthandler.hpp"
#include "items/inventory.hpp"
#include <fea/assert.hpp>

AnyMap<std::string> InspectHandler::invoke(ItemAction action, Item& item, ItemInvoker& invoker, AnyMap<std::string> parameters)
{
    FEA_ASSERT(item.attributes().count("inventory") != 0, "Item has inspect handler but no inventory. My type id is " << item.type().typeId);
    FEA_ASSERT(item.attributes().at("inventory").isOfType<Inventory>(), "Item has inventory but it is not of type Inventory");

    Inventory& inventory = item.attributes().at("inventory").get<Inventory>();

    return {{"inventory", std::cref(inventory)}};
}
