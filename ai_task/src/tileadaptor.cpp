#include "tileadaptor.hpp"
#include "tiles.hpp"

TileAdaptor::TileAdaptor(const Grid<TileType>& map) :
    mMap(map)
{
}

const glm::ivec2 TileAdaptor::getNeighbor(const glm::ivec2& tile, uint32_t index) const
{
    uint32_t skip = index;

    glm::ivec2 up = glm::ivec2(0, -1);
    if(!isSolid(mMap.at(tile + up)))
    {
        if(skip == 0)
            return tile + up;
        --skip;
    }
    
    glm::ivec2 right = glm::ivec2(1, 0);
    if(!isSolid(mMap.at(tile + right)))
    {
        if(skip == 0)
            return tile + right;
        --skip;
    }

    glm::ivec2 down = glm::ivec2(0, 1);
    if(!isSolid(mMap.at(tile + down)))
    {
        if(skip == 0)
            return tile + down;
        --skip;
    }
    
    glm::ivec2 left = glm::ivec2(-1, 0);
    if(!isSolid(mMap.at(tile + left)))
    {
        if(skip == 0)
            return tile + left;
        --skip;
    }

    std::cout << "woops this should'mnt have happened\n";
    exit(2);

    return glm::ivec2();
}

uint32_t TileAdaptor::getNeighborAmount(const glm::ivec2& tile) const
{
    uint32_t amount = 0;

    glm::ivec2 up = glm::ivec2(0, -1);
    if(!isSolid(mMap.at(tile + up)))
        amount++;

    glm::ivec2 right = glm::ivec2(1, 0);
    if(!isSolid(mMap.at(tile + right)))
        amount++;

    glm::ivec2 down = glm::ivec2(0, 1);
    if(!isSolid(mMap.at(tile + down)))
        amount++;
    
    glm::ivec2 left = glm::ivec2(-1, 0);
    if(!isSolid(mMap.at(tile + left)))
        amount++;

    return amount;
}

int32_t TileAdaptor::getStepCost(const glm::ivec2& tileA, const glm::ivec2& tileB) const
{
    return 1;
}

int32_t TileAdaptor::estimateDistance(const glm::ivec2& start, const glm::ivec2& target) const
{
    return static_cast<int32_t>(glm::distance((glm::vec2)start, (glm::vec2)target) * 10.0f);
}

bool TileAdaptor::isSolid(TileType type) const
{
    return type == WALL || type == SPIKES;
}
