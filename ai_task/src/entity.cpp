#include "entity.hpp"
#include "world.hpp"

Entity::Entity(const glm::vec2& size, const fea::Texture& texture, World& world):
    mQuad(size),
    mMoveSpeed(4.0f),
    mWorld(world)
{
    mQuad.setTexture(texture);
    mQuad.setOrigin(mQuad.getSize() / 2.0f);
}

void Entity::setPosition(const glm::vec2& position)
{
    mQuad.setPosition(position);
}

glm::ivec2 Entity::tilePosition() const
{
    return static_cast<glm::ivec2>(mQuad.getPosition() / mQuad.getSize());
}

glm::vec2 Entity::position() const
{
    return mQuad.getPosition();
}

std::vector<fea::RenderEntity> Entity::getRenderInfo() const
{
    return mQuad.getRenderInfo();
}

bool Entity::walkInDirection(const glm::vec2& direction)
{
    auto targetPosition = mQuad.getPosition() + direction * mMoveSpeed;

    auto size = mQuad.getSize() / 2.0f;

    bool collides = mWorld.isSolidAt(targetPosition + size * glm::vec2(0.7f, 0.7f));
    collides = collides || mWorld.isSolidAt(targetPosition + size * glm::vec2(0.7f, -0.7f));
    collides = collides || mWorld.isSolidAt(targetPosition + size * glm::vec2(-0.7f, 0.7f));
    collides = collides || mWorld.isSolidAt(targetPosition + size * glm::vec2(-0.7f, -0.7f));

    if(!collides)
        mQuad.translate(direction * mMoveSpeed);

    return collides;
}

fea::SubrectQuad& Entity::quad()
{
    return mQuad;
}

void Entity::interact()
{
    mWorld.interact(tilePosition());   
}
