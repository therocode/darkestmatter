#pragma once
#include <fea/util.hpp>
#include "jobai/jobsolver.hpp"
#include "glm.hpp"
#include <fea/rendering/glmhash.hpp>
#include "tileadaptor.hpp"

class UnlockDoorSolver : public JobSolver
{
    enum { SEARCHING, GRABBING_KEY};
    public:
        UnlockDoorSolver(const Job& job, const AnyMap<std::string>& external);
        UpdateResult update() override;
        void notifySuccess(AnyMap<std::string> completedData) override;
        void notifyFail(AnyMap<std::string> failedData) override;
    private:
        th::Optional<glm::ivec2> mKeyPosition;
        glm::ivec2 mDoorPosition;
        int32_t mStatus;
        glm::vec2 mCurrentSearchDirection;
};
