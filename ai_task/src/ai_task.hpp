#include <fea/structure.hpp>
#include <fea/render2d.hpp>
#include <fea/userinterface.hpp>
#include "jobai/brain.hpp"
#include "entity.hpp"
#include "world.hpp"
#include "tiles.hpp"
#include <gimgui/data/element.hpp>
#include "drawables/guielement.hpp"

class Ai_task : public fea::Application
{
    public:
        Ai_task();
    protected:
        virtual void loop() override;
    private:
        void handleInput();
        void updateTaskText();
        fea::Window mWindow;
        fea::InputHandler mInputHandler;
        fea::Renderer2D mRenderer;

        fea::Texture mFaceTexture;
        fea::Texture mDungeonTexture;
        fea::Texture mWhiteTexture;
        std::string mCurrentTaskString;
        gim::Font mFont;
        gim::Element mGui;
        GuiElement mGuiDrawable;

        Job mEscapeJob;
        World mWorld;
        Entity mEntity;
        Brain mBrain;
};
