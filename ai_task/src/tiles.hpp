#pragma once

using TileType = int32_t;

enum { EMPTY = 0, WALL, LOCKED_DOOR, SPIKES, METAL_DOOR, KEY, BUTTON, PUSHED_BUTTON, GRASS };
